#!/usr/bin/env python

import subprocess

def get_ipv4(interface):
    """
        Return the IPv4 address of a given interface.

        @param interface: The network interface to retrieve the IP Address from.
    """
    command = "ifconfig {0} | grep inet\ addr | awk '{{ print $2 }}' | awk -F ':' '{{ print $2 }}'".format(interface)
    return subprocess.check_output(command, shell=True).strip()
