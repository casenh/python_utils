#!/usr/bin/env python

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2017, Privasera'

import os
from subprocess import CalledProcessError
from python_utils.files import FileEditor
from python_utils.run import run, run_no_out


def enable_site(config_path, config_name, **opts):
    """
        Upload and enable an Apache site configuration.

        @param envs: Environment variables to pass to Apache.
        @param service_name: Name of the WSGI service.
        @param config_path: Path to the configuration file.
        @param config_file: Name of the configuration file.
        @param server_names: List of Domain names for the server.
        @param cert: Certificate file and file paths as a tuple (cert, key).
        @param log_level: Log level for Apache.
        @param log_path: Path to save Apache log.
        @param error_path: Path to save Apache error log.
        @param wsgi_path: Path of wsgi script.
    """
    config_path = '{0}/{1}'.format(config_path, config_name)
    apache_config = FileEditor(config_path)
    vhost_lines = apache_config.lines_containing('VirtualHost')
    if len(vhost_lines) != 2:
        print('Invalid Apache configuration')
        return

    # Update the wsgi service name
    service_name = opts.get('service_name', None)
    if service_name is not None:
        apache_config.delete_lines_with('WSGIDaemonProcess')
        apache_config.insert_line(vhost_lines[0] + 1, 
                                  '\tWSGIDaemonProcess {0}\n'.format(service_name))

    # Add domain names to the Apache configuration if provided.
    server_names = opts.get('server_names', None)
    if server_names is not None and len(opts['server_names']) > 0:
        apache_config.delete_lines_with('ServerName')
        apache_config.delete_lines_with('ServerAlias')

        apache_config.insert_line(vhost_lines[0] + 1,
                                 '\tServerName {0}\n'.format(server_names[0]))
        for i in range(1, len(server_names)):
            apache_config.insert_line(vhost_lines[0] + 1 + i,
                                      '\tServerAlias {0}\n'.format(server_names[i]))

    # Update the certificate file location
    cert = opts.get('cert', None)
    if isinstance(cert, (list, tuple)) and len(cert) == 2:
        apache_config.delete_lines_with('SSLEngine')
        apache_config.delete_lines_with('SSLCertificateFile')
        apache_config.delete_lines_with('SSLCertificateKeyFile')
        apache_config.insert_line(-1, '\tSSLEngine On\n')
        apache_config.insert_line(-1, '\tSSLCertificateFile {0}\n'.format(cert[0]))
        apache_config.insert_line(-1,'\tSSLCertificateKeyFile {0}\n'.format(cert[1]))

    # Update the log level of apache
    log_level = opts.get('log_level', None)
    if isinstance(log_level, str):
        apache_config.delete_lines_with('LogLevel')
        apache_config.insert_line(-1, '\tLogLevel {0}\n'.format(log_level))

    # Update the Error log location of Apache
    error_path = opts.get('error_path', None)
    if isinstance(error_path, str):
        apache_config.delete_lines_with('ErrorLog')
        apache_config.insert_line(-1, '\tErrorLog {0}/error-apache.log\n'.format(error_path))

    # Update the Access log location of Apache
    log_path = opts.get('log_path', None)
    if isinstance(log_path, str):
        apache_config.delete_lines_with('CustomLog')
        log_entry = '\tCustomLog {0}/access-apache.log combined\n'.format(error_path)
        apache_config.insert_line(-1, log_entry)

    # Update the WSGI script location
    wsgi_path = opts.get('wsgi_path', None)
    if isinstance(wsgi_path, str):
        apache_config.delete_lines_with('WSGIScriptAlias')
        apache_config.insert_line(-1, '\tWSGIScriptAlias / {0}\n'.format(wsgi_path))

    # Update environment variables
    envs = opts.get('environ_vars', None)
    if envs is not None:
        apache_config.delete_lines_with('SetEnv')
        for key, val in envs.iteritems():
            apache_config.insert_line(-1, '\tSetEnv {0} {1}\n'.format(key, val))

    vhost_lines = apache_config.lines_containing('VirtualHost')
    apache_config.delete_line(vhost_lines[1])
    apache_config.append_line('</VirtualHost>')

    run_no_out(['cp', config_path, '/etc/apache2/sites-available/'], sudo=True)
    run_no_out(['a2ensite', config_name], sudo=True)
    return


def disable_site(config_name):
    """
        Disable a site from Apaches configuration if it is enabled.

        @param config_name: Name of Apache configuration file.
    """
    if os.path.isfile('/etc/apache2/sites-enabled/{0}'.format(config_name)):
        run_no_out(['a2dissite', config_name], sudo=True)
    
    try:
        os.remove('/etc/apache2/sites-available/{0}'.format(config_name))
    except OSError as e:
        if e.errno != 2:
            raise e
    return


def add_listen_port(port):
    """
        Add a port for Apache to listen on.

        @param port: Port to add to the Apache configuration file.
    """
    apache_conf = FileEditor('/etc/apache2/ports.conf')
    port_statement = 'Listen {0}'.format(port)
    lines = apache_conf.lines_containing(port_statement)
    if len(lines) == 0:
        # Not the last line since that is vim styling
        apache_conf.insert_line(len(apache_conf) - 1, '{0}\n'.format(port_statement))
    return


def remove_listen_port(port):
    """
        Remove a port from the ports Apache listens on.

        @param port: Port to remove from the configuration file.
    """
    apache_conf = FileEditor('/etc/apache2/ports.conf')
    port_statement = 'Listen {0}'.format(port)
    lines = apache_conf.lines_containing(port_statement)
    if len(lines) != 0:
        for line in lines:
            apache_conf.delete_line(line)
    return


def is_running():
    """ Check if the Apache service is running. """
    # TODO: This doesn't always successfully reload apache. Sometimes it
    # thows CalledProcessError for an unknown reason even if running.
    try:
        output = run(['service', 'apache2', 'status'], sudo=True)
    except CalledProcessError as e:
        print(e)
        return False

    for line in output.split('\n'):
        if 'Active:' in line and 'active' in line:
            return True
    return False


def start_apache():
    """ Start the Apache service. """
    try:
        run_no_out(['service', 'apache2', 'start'], sudo=True)
    except CalledProcessError as e:
        pass


def stop_apache():
    """ Stop the Apache service. """
    try:
        run_no_out(['service', 'apache2', 'stop'], sudo=True)
    except CalledProcessError as e:
        pass


def reload():
    """ Reload the Apache configuration. """
    try:
        run_no_out(['service', 'apache2', 'reload'], sudo=True)
    except CalledProcessError as e:
        start_apache()

    #if not is_running():
    #    start_apache()
    #else:
    #    run(['service', 'apache2', 'reload'], sudo=True)
    #return
