#!/usr/bin/env python

__author__ = 'Abbas Ally, Casen Hunger'
__copyright__ = 'Copyright 2017, Privasera'

from subprocess import CalledProcessError
from python_utils.run import run, run_no_out


def _tunnel_name(suffix):
    """ 
        Return the GRE tunnel name.

        @param suffix: String to append to end of tunnel.
    """
    return 'g_{0}'.format(''.join(suffix.split('.')))


def create_tunnel(src_ip, src_bind_ip, dest_ip, dest_range):
    """ 
        Creates a GRE Tunnel from the origin node to the destination node. 
        
        @param src_ip: Source IP Address requests originate from.
        @param src_bind_ip: IP Address the interface will bind to and listen 
                            for traffic on.
        @param dest_ip: Destination ip address to packets will be sent to.
        @param dest_range: Destination IP Addresses range to tunnel.
    """ 
    tunnel_name = _tunnel_name(dest_ip)
    try:
        if run(['ip', 'tunnel', 'show', tunnel_name], sudo=True) == '':
            run_no_out(['ip', 'tunnel', 'add', tunnel_name, 'mode', 'gre', 'remote', 
                        dest_ip, 'local', src_ip, 'ttl', '255'], sudo=True)
            run_no_out(['ip', 'link', 'set', tunnel_name, 'up'], sudo=True)
            run_no_out(['ip', 'addr', 'add', src_bind_ip, 'dev', tunnel_name], sudo=True)
            run_no_out(['ip', 'route', 'add', dest_range, 'dev', tunnel_name], sudo=True) 

    except CalledProcessError as e:
        print(e)
        return
        
    return


def delete_tunnel(dest_ip):
    """ 
        Delete an existing GRE tunnel. 
        
        @param dest_ip: Destination IP Address packets and sent to.
    """
    tunnel_name = _tunnel_name(dest_ip)

    # Tunnel should exist
    try:
        if run(['ip', 'tunnel', 'show', tunnel_name], sudo=True) == '':
            return

    except CalledProcessError as e:
        print(e)
        return

    # Tunnel should not be running
    try:
        run_no_out(['ip', 'link', 'show', 'up', tunnel_name], sudo=True)
        run_no_out(['ip', 'link', 'set', tunnel_name, 'down'], sudo=True)
    except Exception as e:
        print(e)

    try:
        run_no_out(['ip', 'tunnel', 'del', tunnel_name], sudo=True)
    except Exception as e:
        print(e)
    return


def delete_all_tunnels():
    """ Delete all GRE tunnels. """
    res = run(['ip', 'tunnel', 'show'], sudo=True)
    tunnels = res.split('\n')
    for tunnel in tunnels:
        tunnel_name = tunnel.split(':')[0]

        if 'g_' in tunnel_name and tunnel_name != 'gre0':
            tunnel_id = tunnel_name[2:]
            delete_tunnel(tunnel_id)
    return
