
import sys, re, random, subprocess
import inspect

from python_utils.log import Log
from python_utils.server import PerformanceStats

class TestMethod():

    def __init__(self, args):
        self.module_name = args['module_name']
        self.class_name = args['class_name']
        self.method_name = args['method_name']

    def run_test(self):
        out = ""
        try:
            proc = subprocess.Popen(["python", "{}.py".format(self.module_name), 
                             "{0}.{1}".format(self.class_name, self.method_name)],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            proc.wait()
            out += proc.communicate()[1]
        except Exception as e:
            out += str(e)

        return out

class TestRunner():

    def __init__(self, module_list, run_times, log_path):
        self.module_list = module_list 
        self.run_times = run_times
        self.log_path = log_path

        self.tests = []
        self.populate_tests()
        self.log = Log(self.log_path, 'debug')
        self.log.clear()
        
    def populate_tests(self):
        self.tests = [] 

        # Populate test list
        for t_mod in self.module_list:
            args = {}
            args['module_name'] = t_mod
            test_classes = TestRunner.get_test_classes(t_mod)

            for t_cls in test_classes:
                args['class_name'] = t_cls[0]
                test_methods = TestRunner.get_test_methods(t_cls)

                for t_mthd in test_methods:
                    args['method_name'] = t_mthd

                    test = TestMethod(args)
                    self.tests.append(test)

    def run_tests(self):
        for i in xrange(0, self.run_times):
            rand = random.randint(0, len(self.tests)-1)
            out = self.tests[rand].run_test() 
            label = '{0}.{1}'.format(self.tests[rand].class_name, 
                                     self.tests[rand].method_name)
            self.log.write('debug', label, output=out) 

    @classmethod
    def add_decorator(cls, class_):
        print class_

    @classmethod
    def get_test_classes(cls, module_name):
        test_classes= []

        __import__(module_name)
        classes = inspect.getmembers(sys.modules[module_name], inspect.isclass)
        for c in classes:
            for b in c[1].__bases__:
                if b.__name__ == "TestCase":
                    test_classes.append(c)
                TestRunner.add_decorator(c)


        return test_classes

    @classmethod
    def get_test_methods(cls, test_class):
        test_methods = []

        methods = dir(test_class[1])
        for m in methods:
            if re.match("test.*", m): 
                test_methods.append(m)

        return test_methods

    @classmethod
    def run(cls):
        test_modules= [] 
        run_times = 0

        if len(sys.argv) <= 1:
            stop = "y"
            while stop != "n":
                test_modules.append(raw_input("What class do you want to test: "))
                stop = raw_input("Add more classes (y/n): ").lower
            run_times = int(raw_input("How many tests to randomly run (#): "))
        else:
            for i in xrange(1, len(sys.argv)-1): 
                test_modules.append(sys.argv[i])
                run_times = int(sys.argv[len(sys.argv)-1])

        # TODO: change path from hard coded path
        tr = TestRunner(test_modules, run_times, "../logs/test.log")
        tr.run_tests()
        

