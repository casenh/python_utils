#!/usr/bin/env python

from operator import itemgetter
import os
import string
import random
import subprocess
import sys
import threading
import time
import traceback

from python_utils.http import Request
from python_utils.gnuplot import convert_to_pdf, create_graph
from python_utils.server import _calculate_throughput_vs_latency
from python_utils.test.graph import Graph, EndOfGraphException


class TestDriver():

    def __init__(self, url, rest_graph, num_testers=1, log=None):
        self.tester_list = []
        for i in range(0, num_testers):
            graph = Graph.clone(rest_graph)
            tester = Tester(url, graph, log=log)
            self.tester_list.append(tester)
        return

    def run_test(self):
        self.thread_list = []
        for tester in self.tester_list:
            t = threading.Thread(target=tester.run_test)
            self.thread_list.append(t)

        for t in self.thread_list:
            t.start()

        for t in self.thread_list:
            t.join()
        return

    def write_results(self, path, title):
        write_results(path, self.tester_list, title)
        return


class Tester():

    def __init__(self, url, rest_graph, log=None):
        """
            REQUIRED:
            @param url: URL to make requests to, does not include endpoint (Ex. https://blox.com:6240)
            @param rest_graph: RESTGraph object to walk through for testing

            OPTIONAL:
            @param log: Log object for debugging

            CREATED:
            @attr times: Timings kept for each request made
            @attr errors: Number of error accumulated through testing
        """
        Request.disable_warnings()
        self.url = url
        self.request = Request(url, use_session=True)
        self.graph = rest_graph
        self.log = log
        self.username = None
        self.times = {} 
        self.errors = 0
        return

    @classmethod
    def clone(cls, old_tester):
        t = Tester(old_tester.url, old_tester.graph, log=old_tester.log)
        t.username = old_tester.username
        t.password = old_tester.password
        t.access_token = old_tester.access_token
        t.request.set_access_token(old_tester.access_token)
        return t


    def write_log(self, level, status, *args):
        """ Thin wrapper for checking if log exists """
        if self.log is not None:
            self.log.write(level, status, *args)
        return
   

    def run_test(self):
        """ Run through a RESTGraph object making requests to nodes and taking statistics """
        while True:
            try:
                node = self.graph.get_next_node()

                #TODO: Always create test arguments?
                test_args = node.create_test_object()
                route, args = node.pre_call(self, node, test_args)

                # Some routes may have prerequisites we don't meet yet
                if route == None:
                    self.graph.set_response(args)
                    continue

                response, error = self.time_request(node, node.method, route, args)
                result = node.post_call(self, response, error, args)
                self.graph.set_response(result)

                if node.delay != None:
                    time.sleep(node.delay)
                elif self.graph.delay != None:
                    time.sleep(self.graph.delay)

            except EndOfGraphException as e:
                return

            except Exception as e:
                print("Exception")
                print(e)
                print(traceback.format_exc())
                stack_trace = traceback.format_exc()
                try:
                    self.write_log('error', 'Tester.run_test - exception', e,
                                   stack_trace, route, vars(node))
                    print(route, node.method, node.name)

                except NameError as e:
                    self.write_log('error', 'Tester.run_test - exception', e, stack_trace)
                raise e


    def time_request(self, node, method, url, args):
        response = None
        error = None
        try:
            start = time.time()
            response = self.request.make(method, url, args=args)
            end = time.time()

            key = node.name
            if key not in self.times:
                self.times[key] = {}
            if method not in self.times[key]:
                self.times[key][method] = [{'start': start, 'end': end}]
            else:
                self.times[key][method].append({'start': start, 'end': end})

        except Exception as e:
            if self.log is not None:
                self.log.write('error', 'Tester.time_request - exception', 
                               username=self.username, error=e, node=node, method=method, 
                               url=url, args=args)
            else:
                print('Time Request error')
                error = e
            raise e
        
        return response, error


def write_results(path, tester_list, pdf_title):
    """
        Write the performance metrics from the tests to PNGs and PDFs.
        
        @param path: Path to the directory where files should be saved.
        @param tester_list: List of tester objects that were used for testing.
        @param pdf_title: Title of the PDF that is produced.
    """
    # Calculate timing results across all tester objects used
    results = {}
    latency_list = [] # Used to find throughput vs latency for all reqs
    for tester in tester_list:
        for endpoint, endpoint_results in tester.times.iteritems():
            for method, timing_list in endpoint_results.iteritems():

                key = '{0}-{1}'.format(endpoint, method)
                if key not in results:
                    results[key] = []

                for timing in timing_list:
                    index = len(results[key]) + 1
                    latency = float(timing['end']) - float(timing['start'])
                    results[key].append((index, latency))
                    latency_list.append(('throughput_v_latency', float(timing['start']), latency))

    # Calculate throughput vs latency
    png_list = []
    latency_list.sort(key=lambda tup: tup[1])
    throughput_vs_latency = _calculate_throughput_vs_latency(latency_list)
    data_path = '{0}ThroughputVsLatency.txt'.format(path)
    png_path = '{0}ThroughputVsLatency.png'.format(path)
    create_graph('ThroughputVsLatency', 'Throughput (requests/sec)', 'Latency (s)',
                 throughput_vs_latency, png_path, data_path)
    png_list.append(png_path)

    # Create the graphs
    for label, latency_list in results.iteritems():
        data_path = '{0}{1}.txt'.format(path, label)
        png_path = '{0}{1}.png'.format(path, label)
        create_graph(label, 'Request Number', 'Time (s)', latency_list,
                     png_path, data_path)
        png_list.append(png_path)

    convert_to_pdf(png_list, '{0}/results.pdf'.format(path))
    return

    
def rand_string():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))
