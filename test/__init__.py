#!/usr/bin/env python

__copyright__ = 'Copyright 2016, Privasera'


import random
import string
import sys
import unittest


def assert_args_equal(args, obj):
    """ 
        Check that the keys and values of from args is a subset of the
        keys and values of obj.
    """
    for key, value in args.iteritems():
        try:
            if obj[key] != value:
                print("Unmatched Object Found!")
                print(obj)
                print(args)
                sys.exit(0)

        except IndexError as e:
            print("Missing Keys Found!")
            print(obj)
            print(args)
            sys.exit(0)


def create_random_value(type_, length=10):
    """ Create a random value based on the value type """
    if type_ == "int":
        return random.randint(0, length)
    if type_ == "string":
        return rand_string(length)
    if type_ == "bool":
        return random.choice([True, False])


def rand_string(length):
    """ Create a random alphanumeric string

        REQUIRED:
        @param length: Length of the string to make
    """
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))
