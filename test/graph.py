#!/usr/bin/env python

__copyright__ = 'Copyright 2016, Privasera'


import json
import random

from python_utils.test import create_random_value


class NoChildrenException(Exception):
    def __init__(self):
        return

class EndOfGraphException(Exception):
    def __init__(self):
        return


class Node:
    
    def __init__(self, name, route, method, attributes, pre_call, post_call, label="", color="grey", delay=0, log=None):
        """
            Represents a testing unit for a single REST api call

            REQUIRED:
            @param name: Name to give this particular node (used for graphing)
            @param route: Route that should be tested (URL is excluded. Ex. /api/bubbles)
            @param method: Method to call on route (get, put, post, delete)
            @param attributes: Attributes that should be passed along with request
            @param pre_call: Function to call before request is made
            @param post_call: Function to call after request is completed

            OPTIONAL:
            @param color: Color to fill the node when graphing
            @param label: Label to assign to the node when graphing
            @param delay: Amount of time to sleep after making request
            @param log: Log object for debugging
        """
        self.name = name
        self.route = route
        self.method = method
        self.attributes = attributes
        self.pre_call = pre_call
        self.post_call = post_call
        self.color = color
        self.label = label
        self.delay = delay
        self.log = log

        self.children = []
        return


    def __getitem__(self, item):
        """ Allow indexing for compatability """
        return getattr(self, item)


    def add_child(self, child_node, weight=None):
        """ Add a child node that can be called next """
        self.children.append({'node': child_node, 'weight': weight})
        return


    def get_children(self):
        """ Return the list of children for this node """
        return self.children


    def create_test_object(self):
        """ Create a dictionary of test values to pass as parameters """
        args = {}
        for attribute in self.attributes:
            args[attribute[0]] = create_random_value(attribute[1], attribute[2])
        return args



class Graph:
   
    def __init__(self, node_list, root_node, total_steps, delay=None, log=None):
        """
            Graph representation of a Nodes. Provides methods to easily step through the graph
            and execute each node, collecting performance metrics along the way 

            REQUIRED:
            @param root_node: Root node of the graph
            @param total_steps: Number of steps to make through the graph

            OPTIONAL:
            @param log: Log object for debugging
        """
        self.node_list = node_list
        self.root = root_node 
        self.current = root_node 
        self.previous = root_node
        self.total_steps = total_steps
        self.delay = delay
        self.steps_taken = 0
        self.traversed_list = []
        return

    @classmethod
    def clone(self, old_graph):
        """
            Clone an existing Graph instance.

            @param old_graph: The graph instance to clone.
        """
        graph = Graph(old_graph.node_list, old_graph.root, old_graph.total_steps,
                      delay=old_graph.delay)
        return graph


    def get_next_node(self):
        """ Grab the next child node of the graph. If repeat is set, start back at the root,
            else end the execution

            @raises EndOfGraphException: The graph has run out of nodes to execute or max
                                         steps has been reached
        """
        if self.steps_taken == self.total_steps:
            raise EndOfGraphException
        self.steps_taken += 1

        # Grab the next node based on weights (if provided) or randomly
        current = self.current
        freq = random.randint(0, 99)
        weight_sum = 0
        try:
            child_list = current.get_children()

            # We're a single node graph
            if len(child_list) == 0:
                next_node = current

            for child in child_list:

                if child['weight'] is not None:
                    weight_sum += child['weight']

                # If no weight was assigned, pick a random child and move on
                else:
                    index = random.randint(0, len(child_list) - 1)
                    next_node = child_list[index]['node']
                    break

                if freq < weight_sum:
                    next_node = child['node']
                    break

        except NoChildrenException as e:
            raise EndOfGraphException


        self.current = next_node
        self.previous = current
        return current


    def set_response(self, result):
        """ 
            Set the response for the last executed node for graphing 
            
            @param result: String result to set for the last executed Node
        """
        self.traversed_list.append((self.previous, result))
        return


    def create_legend(self):
        # Create a legend of colors to labels
        legend_map = {}
        legend = []
        for node in self.node_list:
            if node['label'] not in legend_map:
                legend_map[node['label']] = node['color']
                legend.append({'label': node['label'], 'color': node['color']})

        return legend


    def create_traversed_graph(self, results_file):
        """ 
            Create a JSON representation for graphing the graph's acutal execution path 

            @param results_file: Path to save the graph file
        """
        # Create a graph of the actual execution path
        traversed_nodes = []
        traversed_links = []
        index = 0
        for node in self.traversed_list:
            traversed_nodes.append({ 'r': 10, 'fill': node[0].color, 'label': node[0].label, 
                'result': node[1], 'class': node[0].name, 'method': node[0].method, 'route': node[0].route })
            
            if index > 0:
                traversed_links.append({'source': index - 1, 'target': index})
            index += 1
        
        # Save results in D3.js graphable form
        results = { 'nodes': traversed_nodes, 'links': traversed_links, 'legend': self.create_legend() }
        with open(results_file, 'w') as f:
            f.write(json.dumps(results))

        return


    def create_overview_graph(self, results_file):
        """ 
            Create a JSON representation of an overview for the entire graph

            @param results_file: Path to save the graph file
        """
        # Create an overview graph of all nodes and their connections
        graph = []
        nodes = []
        links = []
        index_map = {}
        index = 0
        for node in self.node_list:
            if node == self.root:
                color = 'yellow'
            else:
                color = node.color

            nodes.append({'r': 10, 'fill': node.color, 'label': node.label})
            index_map[node] = index
            index += 1

        for node in self.node_list:
            for child in node.children:
                links.append({'source': index_map[node], 'target': index_map[child['node']]})

        # Save results in D3.js graphable form
        results = { 'nodes': nodes, 'links': links, 'legend': self.create_legend() }
        with open(results_file, 'w') as f:
            f.write(json.dumps(results))

        return


def create_node_list(node_config_list, log=None):
    """ 
        Create the Nodes for testing bubble endpoints 
        
        @param node_config_list: List on Nodes in the graph
        @param log: Log object for debugging
    """
    node_list = []
    for config in node_config_list:

        # Make a clone of the config node so we can delete elements without effecting later tests 
        temp_node = {}
        for key, value in config.iteritems():
            temp_node[key] = value

        for key, value in config['base'].iteritems():

            # Don't overview existing keys
            if key not in temp_node:
                temp_node[key] = value
                temp_node['log'] = log

        del temp_node['base']

        if 'children' in temp_node:
            connection_list = temp_node['children']
            del temp_node['children']
        else:
            connection_list = None

        if 'parents_allowed' in temp_node:
            parents_allowed = temp_node['parents_allowed']
            del temp_node['parents_allowed']
        else:
            parents_allowed = True

        node = Node(**temp_node)
        node.connection_list = connection_list
        node.parents_allowed = parents_allowed

        node_list.append(node)

    # Create children connections
    for node in node_list:

        if node.connection_list is not None:

            for connection in node.connection_list:
                connection = connection.split('.')
                if len(connection) == 1:
                    name = connection[0]
                    method = None

                elif len(connection) == 2:
                    name = connection[0]
                    method = connection[1]

                for child in node_list:

                    if child.name == name and method is None:
                        node.add_child(child)

                    elif child.name == name and child.method == method:
                        node.add_child(child)


        # Connect to all nodes if no children are passed
        else:
            for child in node_list:
                if child != node and child.parents_allowed == True:
                    node.add_child(child)

        
    return node_list


def create_rest_graph(node_list, steps, log=None):
    """ 
        Create the Graph for testing bubble endpoints. Should be a connected 
        graph from the nodes generated from get_rest_nodes()

        @param steps: Number of steps to take through the graph when testing
        @param log: Log object for debugging
    """
    #TODO: Figure out how to add connections
    for node in node_list:
        for child in node_list:
            
            if node != child:
                node.add_child(child)

    return Graph(node_list[0], steps, log=log)
