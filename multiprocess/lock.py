#!/usr/bin/env python

__author__ = 'Casen Hunger'

import redis
import traceback


class BaseLock:
    """ 
        This class defines the interface that all multiprocess compatible
        locks should implement.
    """

    def __init__(self, key):
        """
            Initialize the lock instance. Take in the unique key that will be used
            to identify the lock.

            @param key: The key for the lock instance.
        """
        raise NotImplementedError

    def acquire(self):
        """ Acquire the lock. For now, this function should always block. """
        raise NotImplementedError

    def release(self):
        """ Release the lock. """
        raise NotImplementedError


class RedisLock(BaseLock):

    def __init__(self, key):
        self.key = key
        self.lock = redis.Redis().lock(key)
        return

    def __enter__(self):
        self.acquire()
        return

    def __exit__(self, type, value, traceback):
        self.release()
        return

    def acquire(self):
        self.lock.acquire()
        return

    def release(self):
        self.lock.release()
        return
