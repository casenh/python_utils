#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2016, Casen Hunger'

import json
import redis

from python_utils.cache import CacheUtil


class RedisCache(CacheUtil):
    """ Class is a thin wrapper for beaker caching library """
    expires = None

    @classmethod
    def initialize(cls, args):
        cls.client = redis.StrictRedis(host=args['host'], port=args['port'])
        return

    def __init__(self, prefix):
        """ Initialize a Python Dictionary class """
        self.prefix = prefix
        self.keys = []
        return

    def get_keys(self):
        key_list = []
        raw_key_list = self.client.keys()
        for key in raw_key_list:
            if self.prefix in key:
                key_list.append(key.replace(self.prefix, ''))
        return key_list


    def make_key(self, key):
        new_key = '{}{}'.format(self.prefix, key)
        if new_key not in self.keys:
            self.keys.append(new_key)
        return new_key

    def get_value(self, key):
        """ Get a key from the cache """
        value = self.client.get(self.make_key(key)) 
        if value is not None:
            return json.loads(value)
        else:
            raise KeyError


    def set_value(self, key, value):
        """ Save a value by key to the cache """
        return self.client.set(self.make_key(key), json.dumps(value))

   
    def delete_value(self, key):
        """ Delete an entry from the cache """
        return self.client.delete(self.make_key(key))


    def clear(self):
        """ Remove all entries from the cache """
        #TODO: Don't store keys as array for this. Dictionary may have better performance
        return self.client.delete(self.keys)


    @classmethod
    def clear_cache(cls):
        return cls.client.flushall()
