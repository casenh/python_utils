#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2016, Casen Hunger'

import sys
sys.path.append('../../')
sys.path.append('/home/ubuntu/blox/proxy/')
import src.config


from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options

from python_utils.cache import CacheUtil


class BeakerCache(CacheUtil):
    """ Class is a thin wrapper for beaker caching library """
    cache_manager = None
    expires = None
    type_ = None

    @classmethod
    def initialize(cls, args):
        if 'expires' in args:
            cls.expires = args['expires']
            del args['expires']
        #if 'type' in args:
        #    cls.type_ = args['type']
        
        if cls.cache_manager is None:
            cls.cache_manager = CacheManager(**parse_cache_config_options(args))
        return


    def __init__(self, template):
        """ Initialize a beaker instance

            REQUIRED:
            @param template: Cache template to retrieve
            @arg cache.type: Type of cache backing to use
            @arg cache.lock_dir: Lock directory to use for caching

            OPTIONAL:
            @arg cache.data_dir: Data directory to use for caching
            @arg memcache_module: Module of MemCached to use
            @arg encrypt_key: Key for AES cipher
            @arg validate_key: Valuidation key to sign AES encrypted data
        """
        self.cache = BeakerCache.cache_manager.get_cache(template, expire=BeakerCache.expires)
        return

    def get(self, key):
        """ Get a key from the cache """
        return self.cache.get(key)


    def set(self, key, value):
        """ Save a value by key to the cache """
        return self.cache.set(key, value)


    def clear(self):
        """ Remove all entries from the cache """
        pass


if __name__ == '__main__':
    BeakerCache.initialize(src.config.CACHE_ARGS)
    cache = BeakerCache('test')
    cache.set('test', 'TESTVALUE')
