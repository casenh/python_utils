#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2016, Casen Hunger'

import json
import os

from python_utils.cache import CacheUtil


class FileCache(CacheUtil):
    """ Class is a thin wrapper for beaker caching library """
    expires = None
    cache_dir = None

    @classmethod
    def initialize(cls, args):
        """ Initialize a FileCache class """
        cls.cache_dir = args['cache_dir']
        return

    def __init__(self, template):
        template = '%s.cache' % (template)
        template_path = '%s/%s' % (FileCache.cache_dir, template)
        if not os.path.isfile(template_path):
            with open(template_path, 'w') as f:
                    json.dump({}, f)

        self.template_path = template_path
        return

    def get_value(self, key):
        """ Get a key from the cache """
        with open(self.template_path, 'r') as f:
            try:
                cache = json.load(f)
            except ValueError as e:
                cache = {}

        return cache[key]

    def set_value(self, key, value):
        """ Save a value by key to the cache """
        with open(self.template_path, 'r') as f:
            try:
                cache = json.load(f)
            except ValueError as e:
                cache = {}
        cache[key] = value
        
        with open(self.template_path, 'w') as f:
            json.dump(cache, f)
        return

    
    def delete_value(self, key):
        """ Delete a value by key from the cache """
        with open(self.template_path, 'r') as f:
            try:
                cache = json.load(f)
            except ValueError as e:
                return
        try:
            del cache[key]
        except KeyError as e:
            return
        
        with open(self.template_path, 'w') as f:
            json.dump(cache, f)
        return


    def clear(self):
        """ Remove all entries from the cache """
        with open(self.template_path, 'w') as f:
            json.dump({}, f)
        return


    @classmethod
    def clear_cache(cls):
        pass
