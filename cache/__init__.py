#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2016, Casen Hunger'


import errno
import os 

from abc import ABCMeta, abstractmethod
from python_utils.run import run


class CacheUtil:
    __metaclass__ = ABCMeta 
    """
        Abstract Class for handling caching of data 
    """

    @abstractmethod
    def get_value():
        raise NotImplementedError        
  
    @abstractmethod
    def set_value():
        raise NotImplementedError

    @abstractmethod
    def clear():
        raise NotImplementedError
