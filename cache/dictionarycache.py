#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2016, Casen Hunger'

from python_utils.cache import CacheUtil


class DictionaryCache(CacheUtil):
    """ Class is a thin wrapper for beaker caching library """
    expires = None
    cache = {}
    locks = {}

    @classmethod
    def initialize(cls, args):
        return

    def __init__(self, template):
        """ Initialize a Python Dictionary class """
        if template not in DictionaryCache.cache:
            DictionaryCache.cache[template] = {}
        self.template = template
        return

    def get_value(self, key):
        """ Get a key from the cache """
        return DictionaryCache.cache[self.template][key]


    def set_value(self, key, value):
        """ Save a value by key to the cache """
        DictionaryCache.cache[self.template][key] = value
        return

   
    def delete_value(self, key):
        """ Delete an entry from the cache """
        del DictionaryCache.cache[self.template][key]
        return


    def clear(self):
        """ Remove all entries from the cache """
        DictionaryCache.cache[self.template] = {}
        return


    @classmethod
    def clear_cache(cls):
        cls.cache = {}
        return
