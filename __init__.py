#!/usr/bin/env python

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'

import md5 
import random
import string
import time

def create_id(length=32, numerals=True):
    """
        Creates random ID 32 characters long
    """
    random.seed(time.time())
    if numerals == True:
        return md5.new(str(random.random())).hexdigest()[0:length]
    else:
        return ''.join(random.choice(string.ascii_uppercase) for _ in range(length))
