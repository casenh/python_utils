#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'

import json
import MySQLdb as _mysql
import _mysql_exceptions
import os
import io
import subprocess

# Disable warning about not being able to create an existing database
from warnings import filterwarnings, resetwarnings
filterwarnings('ignore', category=_mysql.Warning)

from python_utils.database import DBConnection
from python_utils.database import error as db_error 
from python_utils.run import run


def handle_request(func):
    def wrapper(self, *args, **kwargs):
        connect(self)
        result = func(self, *args, **kwargs)
        disconnect(self)
        return result
    return wrapper


class MySQLConnection(DBConnection):
    
    # For logging MySQL Actions/Errors
    logger = None
    username = None
    password = None
    host = None

    @classmethod
    def init(cls, username, password, host, logger=None):
        cls.username = username
        cls.password = password
        cls.host = host
        cls.logger = logger
        return
   
    def __init__(self, database=None):
        """ 
            Create a connection to the given database.
        
            @return: DBConnection object connected to the database
        """
        try:
            if database is not None:
                self.database = database

        except _mysql_exceptions.OperationalError as e:
            # Unable to connect
            if e[0] == 1049:
                raise db_error.ConnectionError
        return

    def log(self, level, status, **kwargs):
        if self.logger is not None:
            self.logger.write(level, status, **kwargs)
        return

    def create_table(self, table, statement=None, **key_dict):
        self.create_database(self.database)
        connect(self)

        cmd = 'CREATE TABLE {0} ('.format(table)
        for key, value in key_dict.iteritems(): 
            cmd = '{0} {1} {2},'.format(cmd, key, value)
        if statement is not None:
            cmd = '{0} {1},'.format(cmd, statement)

        # Remove the last trailing ","
        cmd = list(cmd)
        cmd[-1] = ')'
        _execute(self, ''.join(cmd))
        return

    
    def update_table(self, table, **kwargs):
        self.create_database(self.database)
        connect(self)

        rows = self.get_rows(table, None)
        self.delete_table(table)
        self.create_table(table, **kwargs)
        for row in rows:
            new_row = {}
            for key, value in row.iteritems():
                if key in kwargs:
                    new_row[key] = value
            self.create_row(table, new_row)
        return

    def delete_table(self, table):
        self.create_database(self.database)
        connect(self)
        cmd = 'DROP TABLE IF EXISTS ' + table
        _execute(self, cmd)
        return


    # TODO: Commands should come in as an array of tuples, not a dictionary
    def _cmd_add_values(self, cmd, values, delimeter=',', operator='=', query=False):
        args = []
        for key, value in values.iteritems():
            
            if operator == '=':
                if value is None and query == True:
                    cmd += '{} is %s {} '.format(key, delimeter)
                else:
                    cmd += '{}=%s {} '.format(key, delimeter)

            elif operator == 'IN':
                cmd += 'find_in_set(cast({} as char), %s) {} '.format(key, delimeter)

                str_array = ''
                for val in value:
                    str_array = '{0}{1},'.format(str_array, val)
                value = str_array[:-1]

            elif operator == '>=':
                if value is None and query == True:
                    cmd += '{} is %s {} '.format(key, delimeter)
                else:
                    cmd += '{}>=%s {} '.format(key, delimeter)

            args.append(value)
                
        # Protect against empty values
        if len(values.keys()) > 0:
            cmd = cmd[:-len(delimeter)-1]
        return cmd, args

    @handle_request
    def get_rows(self, table, values=None, delimeter='and', operator='='):
        '''
            TODO: Allow multiple search parameters
        '''
        cmd = 'SELECT * FROM {}'.format(table)
        if values is not None:
            cmd += ' WHERE '
            cmd, args = self._cmd_add_values(cmd, values, delimeter=delimeter, operator=operator, query=True)
            _execute(self, cmd, args=args)
        else:
            _execute(self, cmd)
        result = self.cursor.fetchall() 
        return result

    @handle_request
    def create_row(self, table, values):
        cmd = 'INSERT INTO ' + table + ' SET '
        cmd, args = self._cmd_add_values(cmd, values)
        _execute(self, cmd, args=args, commit=True)
        lastrow = self.cursor.lastrowid
        return lastrow

    @handle_request
    def update_row(self, table, identifiers, values, delimeter='and'):
        cmd = 'UPDATE {} SET '.format(table)
        cmd, args = self._cmd_add_values(cmd, values)
        cmd += ' WHERE '
        cmd, select_args = self._cmd_add_values(cmd, identifiers, delimeter=delimeter)
        args += select_args
        _execute(self, cmd, args=args, commit=True)
        rowcount = self.cursor.rowcount
        return rowcount

    @handle_request
    def test_and_set(self, table, identifiers, attribute, allowed_values, new_value):
        """
            Conditionally update a row in the database if a certain value holds. The
            format is:

            UPDATE table SET attr="value" WHERE FIND_IN_SET(table, "list,of,allowed,values") WHERE ID=1;

            @param table: Table within the database to scope to.
            @param identifiers: Extra identifiers to identify the row to update.
            @param attribute: The column we are testing.
            @param allowed_values: A list of acceptable values for the column.
            @param new_value: New value to update the column with.
        """
        if len(allowed_values) < 1:
            return -1

        args = []
        cmd = 'UPDATE {0} SET {1}=%s WHERE FIND_IN_SET({0}.{1}, %s)'.format(table, attribute)
        args.append(new_value)

        # The list of allowed values we provide to mysql should be a single string.
        search_string = ''
        for value in allowed_values:

            # We need to cast True/False here since FIND_IN_SET requires a string
            if value is True:
                value = '1'
            elif value is False:
                value = '0'
            search_string = '{0},{1}'.format(search_string, value)

        search_string = search_string[1:]
        args.append(search_string)

        # Lastly, add the additional identifiers
        if len(identifiers.keys()) > 0:
            cmd = '{0} AND '.format(cmd)
            cmd, _args = self._cmd_add_values(cmd, identifiers, query=True)
            args += _args
        _execute(self, cmd, args=args, commit=True)
        return self.cursor.rowcount


    @handle_request
    def delete_row(self, table, identifiers, delimeter='and'):
        cmd = 'DELETE FROM {} WHERE '.format(table)
        cmd, args = self._cmd_add_values(cmd, identifiers, delimeter=delimeter, query=True)
        _execute(self, cmd, args=args, commit=True)
        lastrow = self.cursor.lastrowid
        return lastrow

    @classmethod
    def database_exists(cls, database):
        """ 
            Check if a database exists in MySQL

            @param database: Database name to check for
        """
        # Could also use
        #SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'DBName'
        try:
            connection = _mysql.connect(cls.host, cls.username, cls.password, database)
            connection.close()
            return True
        except Exception as e:
            return False


    @classmethod
    def create_connection(cls):
        # Create general connection for admin functionality
        connection = _mysql.connect(cls.host, cls.username, cls.password)
        cursor = connection.cursor(_mysql.cursors.DictCursor)
        return connection

    @classmethod
    def create_database(cls, database):
        db = MySQLConnection()
        connect(db)
        cmd = 'CREATE DATABASE IF NOT EXISTS {}'.format(database)
        _execute(db, cmd, commit=True)
        disconnect(db)
        return

    @classmethod
    def drop_database(cls, database):
        db = MySQLConnection()
        connect(db)
        cmd = 'DROP DATABASE IF EXISTS {}'.format(database)
        _execute(db, cmd, commit=True)
        disconnect(db)
        return

    @classmethod
    def create_user(cls, username, ipaddress, password, database, table, permissions):
        """ 
            Create a user for the database. Permissions is similar to chmod
            where bits represent read,write instead of read,write,execute.
            Most significant bit is database permission and least significant is
            table permission.

            @param username: Username to assign to the user.
            @param ipaddress: IP Address accepted for the user.
            @param password: The user's password.
            @param database: Database to allow access to.
            @param table: Table to allow access to.
            @param permissions: Permissions to allow (ex. rr, rw, wr, ww).
        """
        # Check for an already existing user
        cmd = 'select User from mysql.user where User=%s and Host=%s'
        args = [username, ipaddress]

        db = MySQLConnection()
        connect(db)
        _execute(db, cmd, args=args, commit=True)
        result = db.cursor.fetchall() 
        if len(result) > 0:
            return False

        cmd = "create user '{0}'@'{1}' identified by '{2}'".format(username, ipaddress, password)
        _execute(db, cmd, commit=True)
        db.update_permissions(username, ipaddress, database, table, permissions)
        disconnect(db)
        return True

    @classmethod
    def delete_user(cls, username, ipaddress='%'):
        db = MySQLConnection()
        connect(db)
        del_cmd = "DROP USER '{0}'@'{1}';".format(username, ipaddress)
        flush_cmd = "FLUSH PRIVILEGES"
        try:
            _execute(db, flush_cmd, commit=True)
            _execute(db, del_cmd, commit=True)
        except db_error.UserDeletionError as e:
            _execute(db, flush_cmd, commit=True)
            _execute(db, del_cmd, commit=True) 
        _execute(db, flush_cmd, commit=True)
        disconnect(db)
        return

    @classmethod
    def update_permissions(cls, username, ipaddress='%', database='*', table='*', permissions='rr'):
        #TODO: Check if the user already exists. Gives an error if not
        if len(permissions) != 2:
            raise ValueError('Invalid permissions')

        privileges = 'CREATE, INSERT, SELECT, ALTER, INDEX'
        if permissions[0] == 'w':
            privileges = '%s, DROP' % (privileges)
        if permissions [1] == 'w':
            privileges = '%s, DELETE, UPDATE' % (privileges)
        cmd = "GRANT {0} ON {1}.{2} TO '{3}'@'{4}'".format(privileges, database, table, 
                                                           username, ipaddress)

        db = MySQLConnection(None)
        connect(db)
        _execute(db, cmd, commit=True)
        disconnect(db)
        return

    @classmethod
    def copy_database(cls, src_database, dest_database):
        """ 
            Copy the contents of an existing database into a new database. If
            the new database does not already exist, it will be created.

            @param src_database: Source database to copy
            @param dest_database: New name to copy database to

            @ret boolean : True if it correctly executed and False if it failed
        """
        # Check if src_database exists and dest_database does not exist
        if not cls.database_exists(src_database):
            return False

        dump_buffer = io.BytesIO()

        try:
            # XXX: We don't want to drop the table if it already exists.
            dump = run(['mysqldump', '--skip-add-drop-table', '-u', cls.username, 
                        '--password={}'.format(cls.password), src_database]) 
            dump_buffer.write(dump)
            dump_buffer.seek(0)

            if not cls.database_exists(dest_database):
                run(['mysqladmin', '-u', cls.username, '--password={}'.format(cls.password), 
                     'create', dest_database])

            upload_new_db = run(['mysql','-u', cls.username, '--password={}'.format(cls.password), 
                                 dest_database], stdin=subprocess.PIPE, async_=True)
            upload_new_db.communicate(dump_buffer.read())
            upload_new_db.wait()

        except Exception as e:
            return False

        finally:
            dump_buffer.close()

        return True


    @classmethod
    def get_all_databases(cls):
        """ Drop all databases from MySQL. """
        db = MySQLConnection(None)
        connect(db)
        _execute(db, 'SHOW DATABASES', commit=True)
        db_list = db.cursor.fetchall()
        db_list = [d['Database'] for d in db_list]
        disconnect(db)
        return db_list


    @handle_request
    def backup_database(self, backup_file):
        """ Create a backup file for a database. """
        #XXX: We cannot use mysqldump here because there are times where we will need
        # to simply add a column to a table. mysqldump won't let us do that, so we need
        # to manually.
        if self.database is None:
            return

        # Check that the password file exists to avoid writing to terminal
        #home = os.path.expanduser('~')
        #config_location = '{0}/.my.cnf'.format(home)
        #if not os.path.exists(config_location):
        #    with open(config_location, 'w') as f:
        #        f.write('')
        #        run(['chmod', '600', config_location])
        #        f.write('[mysql]\nuser={0}\npassword={1}\n'.format(self.username, self.password))
        #        f.write('[mysqldump]\nuser={0}\npassword={1}'.format(self.username, self.password))

        #output = run(['mysqldump', '-u', self.username, self.database])
        #os.remove(config_location)

        #with open(backup_file, 'w') as f:
        #    f.write(output)
        
        backup = {} # Dictionary to hold the backup
        
        _execute(self, 'SHOW tables', commit=True)
        tables = self.cursor.fetchall()
        for entry in tables:
            # Index into dictionary is Tables_in_"database"
            table = entry['Tables_in_{}'.format(self.database)]

            # Get table information
            _execute(self, 'DESCRIBE {}'.format(table), commit=True)
            description = self.cursor.fetchall()

            # Get rows
            _execute(self, 'SELECT * from {}'.format(table), commit=True)
            rows = self.cursor.fetchall()

            backup[table] = {'description': description, 'rows': rows}
            
        with open(backup_file, 'w') as f:
            json.dump(backup, f)
            
        return


    def restore_database(self, backup_file):
        """ Restore a database from a backup file """
        #XXX: We cannot use mysql or mysqldump here either. See backup_database() above.
        if self.database is None:
            return

        # Check that the password file exists to avoid writing to terminal
        #home = os.path.expanduser('~')
        #config_location = '{0}/.my.cnf'.format(home)
        #if not os.path.exists(config_location):
        #    with open(config_location, 'w') as f:
        #        f.write('')
        #        run(['chmod', '600', config_location])
        #        f.write('[mysql]\nuser={0}\npassword={1}\n'.format(self.username, self.password))
        #        f.write('[mysqldump]\nuser={0}\npassword={1}'.format(self.username, self.password))

        #p = run(['mysql', '-u', self.username, self.database], async_=True)
        #reset_cmd = 'use {0}; source {1};'.format(self.database, backup_file)
        #resp = p.communicate(input=reset_cmd)
        #p.wait()
        #os.remove(config_location)

        with open(backup_file, 'r') as f:
            backup = json.load(f)

        for table, data in backup.iteritems():
            connect(self)

            # Create table only if it does not exist
            query = 'SELECT * from information_schema.tables WHERE table_name="{}"'.format(table)
            _execute(self, query, commit=True)
            results = self.cursor.fetchall()

            # Make sure the table is not an existing system table
            table_exists = False
            for result in results:
                if result['TABLE_SCHEMA'] == self.database:
                    table_exists = True
                    break

            if not table_exists:
                columns = {}
                for column in data['description']:
                    if column['Field'] != 'ID':
                        columns[column['Field']] = column['Type']
                self.create_table(table, **columns)

            # Parse out the current rows of the Table to check for 
            # added/removed columns.
            _execute(self, 'DESCRIBE {}'.format(table), commit=True)
            current_description = self.cursor.fetchall()
            current_columns = []
            for entry in current_description:
                for key, value in entry.iteritems():
                    if key == 'Field':
                        current_columns.append(value)

            # Restore the rows in the table
            for row in data['rows']:
                # Watch for existing entries
                try:
                    if len(self.get_rows(table, {'ID': row['ID']})) != 0:
                        continue
                except KeyError as e:
                    pass

                # Parse out columns that may no longer exist from the data
                row_params = {}
                for backup_column, value in row.iteritems():
                    if backup_column not in current_columns:
                        continue
                    row_params[backup_column] = value

                # Reinsert the parsed row from the backed up data
                self.create_row(table, row_params) 

            disconnect(self)
        return

    def disconnect(self):
        try:
            self.cursor.close()
            self.connection.close()
        except Exception as e:
            pass
        return



def disconnect(db):
    try:
        db.cursor.close()
        db.connection.close()

    except Exception as e:
        pass #TODO: Only want to catch "closing a closed connection"
    return


def connect(db):
    try:
        if hasattr(db, 'database'):
            db.connection = _mysql.connect(db.host, db.username, db.password, db.database)
        else:
            db.connection = _mysql.connect(db.host, db.username, db.password)
        db.cursor = db.connection.cursor(_mysql.cursors.DictCursor)

    except Exception as e:
        raise
    return


def _execute(db, cmd, args=None, commit=False):
    db.log('debug', 'db _execute', cmd=cmd, args=args)

    try:
        with db.connection:
            # Query sanitization
            if args is not None:
                db.cursor.execute(cmd, args)
            else:
                db.cursor.execute(cmd)
            db.connection.commit()
        return
    # Disconnect and try again
    except Exception as exception: 
        db.log('error', 'db _execute - exception', e=exception, cmd=cmd, args=args) 
        # Database Exsists 
        if exception[0] == 1007: 
            raise db_error.DBCreationError('DB Exists') # Cannot drop user 
        # TODO: Should have separate creation and deletion errors 
        if exception[0] == 1396: 
            raise db_error.UserDeletionError 
        print(exception)
        raise db_error.ExecutionError
