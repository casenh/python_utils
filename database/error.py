#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


class CmdSyntaxError(Exception):

    def __init__(self, message=None):
        if message is None:
            message = 'Incorrect database syntax'
        super(CmdSyntaxError, self).__init__(message)


class ConnectionError(Exception):

    def __init__(self, message=None):
        if message is None:
            message = 'MySQL server has gone away'
        super(ConnectionError, self).__init__(message)


class ExecutionError(Exception):

    def __init__(self, message=None):
        if message is None:
            message = ''
        super(ExecutionError, self).__init__(message)


class UserCreationError(Exception):

    def __init__(self, message=None):
        if message is None:
            message = 'Unable to create user in database'
        super(UserCreationError, self).__init__(message)


class UserDeletionError(Exception):

    def __init__(self, message=None):
        if message is None:
            message = 'Unable to delete user from database'
        super(UserDeletionError, self).__init__(message)


class DBCreationError(Exception):

    def __init__(self, message=None):
        if message is None:
            message = 'Unable to create database'
        super(DBCreationError, self).__init__(message)
