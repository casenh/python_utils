#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


import bson
from bson import ObjectId, json_util
import json
from pymongo import MongoClient

from python_utils.database import DBConnection
from python_utils.database.error import ConnectionError, DBCreationError
from python_utils.run import run


class MongoDBConnection(DBConnection):

    # Create general connection for admin functionality
    logger = None
    username = None
    password = None
    host = None
    port = None
    client = None

    @classmethod
    def init(cls, username, password, host, port, logger=None):
        cls.username = username
        cls.password = password
        cls.host = host
        cls.port = int(port)
        cls.logger = logger
        cls.client = MongoClient(cls.host, cls.port)
        return
   
    def __init__(self, database=None):
        """ Create a connection to the given database
        
            @return: DBConnection object connected to the database
        """
        try:
            if database is not None:
                self.database = MongoDBConnection.client[database] 

        except Exception as e:
            raise e

    @classmethod
    def log(cls, level, status, *args):
        if cls.logger is not None:
            cls.logger.write(level, status, *args)
        return

    def create_table(self, table, **kwargs):
        """ Mongo tables can be created on the fly """
        pass


    def delete_table(self, table):
        """ Delete all of the entries in a table """
        self.database[table].delete_many({})
        return


    def get_rows(self, table, values=None, delimeter='and', operator='='):
        return list(self.database[table].find(values))


    def create_row(self, table, values):
        return self.database[table].insert_one(values).inserted_id

    def update_row(self, table, identifiers, values):
        self.database[table].update_one(identifiers, {'$set': values})
        return

    def delete_row(self, table, identifiers, delimeter='and'):
        return self.database[table].delete_many(identifiers)

    @classmethod
    def database_exists(cls, database):
        """ 
            Check if a database exists in Mongo

            @param database: Database name to check for
        """
        cls.log('debug', 'MongoDB.database_exists - start', database)

        client = MongoClient(cls.host, cls.port)
        if database in client.database_names():
            return True
        else:
            return False

        cls.log('debug', 'MongoDB.database_exists - finish', database)
        return

    @classmethod
    def copy_database(cls, src_database, dest_database):
        """ 
            Copy and existing database to a new name

            @param src_database: Source database to copy
            @param dest_database: New name to copy database to
        """
        cls.log('debug', 'MongoDB.copy_database - start', src_database, dest_database)
        client = MongoClient(cls.host, cls.port)
        client.admin.command('copydb', fromdb=src_database, todb=dest_database, fromhost=cls.host)
        cls.log('debug', 'MongoDB.copy_database - finish')
        return

    @classmethod
    def create_database(cls, database):
        """ Databases can be created on the fly with mongo """
        cls.log('debug', 'MonogoDB.create_database - start', database)
        client = MongoClient(cls.host, cls.port)
        db = client[database]
        db['initTable'].insert_one({'init': 'init'})
        cls.log('debug', 'MonogoDB.create_database - finish', database)
        return

    @classmethod
    def drop_database(cls, database):
        cls.log('debug', 'MonogoDB.drop_database - start', database)
        client = MongoClient(cls.host, cls.port)
        result = client.drop_database(database)
        cls.log('debug', 'MonogoDB.drop_database - finish', database, result)
        return 

    @classmethod
    def create_user(cls, username, password, database, table, permissions):
        """
            Create a user for the database
        """
        if permissions == 'rr': #TODO: Give table level granularity
            permissions = True
        else:
            permissions = False
        cls.client[database].add_user(username, password, read_only=permissions)
        return

    @classmethod
    def delete_user(cls, username, ipaddress, database):
        return cls.client[database].remove_user(username)

    @classmethod
    def update_permissions(cls, usernmae, database='*', table='*', permissions='rr'):
        return

    def backup_database(self, backup_file):
        """ Create a backup of the database into a file """
        if self.database is None:
            return False
        backup = {}

        collection_list = self.database.collection_names()
        default_collections = ['system.indexes']
        for collection in collection_list:

            # Ignore the Mongo created collections
            if collection in default_collections:
                continue

            document_list = []
            for document in self.database[collection].find():
                document_list.append(document)
            backup[collection] = document_list

        with open(backup_file, 'w') as f:
            # Use the bson utilities, default JSON encoder can't
            f.write(json_util.dumps(backup))

        return

    def restore_database(self, backup_file):
        """ Restore a database from a given backup file """
        with open(backup_file, 'r') as f:
            backup = json_util.loads(f.read())

        for collection, document_list in backup.iteritems():
            for document in document_list:
                if self.get_rows(collection, document).count() == 0:
                    self.create_row(collection, document)
        return


    @classmethod
    def backup_all_dbs(cls, backup_file):
        """ Create a backup of all database instances. """
        run(['mongodump', '--host', cls.host, '--port', cls.port, '--out', backup_file])
        return

    @classmethod
    def restore_all_dbs(cls, backup_file):
        """ Restore all database instances. """
        run(['mongorestore', backup_file])
        return
