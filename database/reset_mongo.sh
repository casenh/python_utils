#!/bin/sh

echo "show dbs;" > list_dbs.js
mongo 10.0.0.1 < "list_dbs.js" | awk '{ print $1 }' | grep [0-9] | xargs -I {} echo -e "use {};\ndb.dropDatabase();" > drop_dbs.js
mongo 10.0.0.1 < "drop_dbs.js"

rm ./list_dbs.js
rm ./drop_dbs.js
