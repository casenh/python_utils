#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


from abc import ABCMeta, abstractmethod

class DBConnection():
   
    def connection(self):
        raise NotImplementedError


    def disconnect(self):
        raise NotImplementedError
    

    def create_database(database):
        """ Create a new database.
    
            @return: True if the database exists or if it was successfully created.
                     False otherwise.
        """
        raise NotImplementedError


    def create_user(username, password):
        """
            Create a user for the database
        """
        raise NotImplementedError
    

    def create_table(self, table, **kwargs):
        raise NotImplementedError

    
    def delete_table(self, table):
        raise NotImplementedError
    

    def get_rows(self, table, values=None, delimeter='and', operator='='):
        """
            TODO: Allow multiple search parameters
        """
        raise NotImplementedError

    
    def create_row(self, table, values):
        raise NotImplementedError

    
    def update_row(self, table, identifiers, values):
        raise NotImplementedError

    
    def delete_row(self, table, identifiers, delimeter='and'):
        raise NotImplementedError
