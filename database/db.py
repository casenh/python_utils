#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


import MySQLdb as _mysql
import _mysql_exceptions

from python_utils.database.error import ConnectionError, DBCreationError
from src import log 



class DBConnection():
   
    def __init__(self, database=None):
        ''' Create a connection to the given database
        
            @return: Connection to the database
        '''
        try:
            if database == None:
                self.database = database
            self.connect()
        except _mysql_exceptions.OperationalError as e:
            print("ERROR connecting")
            # Unable to connect
            if e[0] == 1049:
                raise ConnectionError


    def connect(self):
        # TODO: No password here!
        self.connection = _mysql.connect('localhost', 'root', '1610MNKB', self.database)
        self.cursor = self.connection.cursor(_mysql.cursors.DictCursor)
        return

    def disconnect(self):
        self.cursor.close()
        self.connection.close()
        return


    def init_database_daemon():
        ''' Initializes the MySQL database. Makes sure the server
            is up and running.
    
            @return: True if daemon is already running or was started.
        '''
        pass
    
    
    def create_db(database):
        ''' Attempt to create a database.
    
            @return: True if the database exists or if it was successfully created.
                     False otherwise.
        '''
        pass


    def create_user(username, password):
        '''
            Create a user for the database
        '''
        cmd = "CREATE USER '%s'@'localhost' IDENTIFIED BY '%s'" % (username, password)
        self._execute(cmd)
        return
    
    
    def create_table(self, table, **kwargs):
        cmd = 'CREATE TABLE ' + table + \
                ' (ID int NOT NULL AUTO_INCREMENT, PRIMARY KEY (ID)'
        for key, value in kwargs.iteritems():
            cmd += ',' + key + ' ' + value
        cmd += ')'
        self._execute(cmd)


    def delete_table(self, table):
        cmd = 'DROP TABLE IF EXISTS ' + table
        self._execute(cmd)


    # TODO: Commands should come in as an array of tuples, not a dictionary
    def _cmd_add_values(self, cmd, values, delimeter=',', operator='='):
        for key, value in values.iteritems():
            
            if value is None:
                value = ""
            if not isinstance(value, basestring):
                value = str(value)

            if operator == '=':
                cmd = '%s%s="%s" %s ' % (cmd, key, value, delimeter)
            if operator == 'IN':
                cmd = '%sfind_in_set("%s", cast(%s as char)) %s' % (cmd, value, key, delimeter)
                
        # Protect against empty values
        if len(values.keys()) > 0:
            cmd = cmd[:-len(delimeter)-1]
        return cmd

    def _execute(self, cmd, commit=False):
        try:
            log.write('debug', 'db _execute', cmd=cmd)
            self.connect()
            with self.connection:
                self.cursor.execute(cmd) #TODO Sanitization: should be .execute("SELECT %s, $s...", (arg1, arg2...))
                if commit == True:
                    self.connection.commit()
        except _mysql_exceptions.IntegrityError as e:
            log.write('debug', 'db _execute IntegrityError', exception=e, cmd=cmd)
            # Duplicate entry
            if e[0] == 1062:
                print("Duplicate Error")
                raise DuplicateError
            if e[0] == 2006:
                #TODO: Raise an error here
                print('Mysql server has gone away')
                try:
                    self.connect()
                    self._execute(cmd)
                except:
                    pass
        except _mysql_exceptions.ProgrammingError as e:
            log.write('debug', 'db _execute', exception=e, cmd=cmd)
            # Incorrect syntax
            if e[0] == 1064:
                print("Syntax Error")
                print(cmd)
                raise _mysql_exceptions.CmdSyntaxError
        except _mysql_exceptions.OperationalError as e:
            log.write('debug', 'db _execute', exception=e, cmd=cmd)
            print('Operational Error')
            print(e)
            try: #TODO: is this one or above 2006 error necessary?
                print('Connecting')
                self.connect()
                self._execute(cmd)
            except:
                print('Connection failed')
                raise
                pass
        except Exception as e:
            log.write('debug', 'db _execute Exception', exception=e, cmd=cmd)
   

    def get_rows(self, table, values=None, delimeter='and', operator='='):
        '''
            TODO: Allow multiple search parameters
        '''
        cmd = 'SELECT * FROM ' + table
        if values is not None:
            cmd = self._cmd_add_values(cmd + ' WHERE ', values, delimeter=delimeter, operator=operator)
        self._execute(cmd)
        return self.cursor.fetchall()


    def create_row(self, table, values):
        cmd = 'INSERT INTO ' + table + ' SET '
        cmd = self._cmd_add_values(cmd, values)
        self._execute(cmd, commit=True)
        return self.cursor.lastrowid

    def update_row(self, table, identifiers, values):
        cmd = 'UPDATE ' + table + ' SET '
        cmd = self._cmd_add_values(cmd, values)
        cmd = cmd + ' WHERE '
        cmd = self._cmd_add_values(cmd, identifiers)
        self._execute(cmd, commit=True)
        return

    def delete_row(self, table, identifiers, delimeter='and'):
        cmd = 'DELETE FROM ' + table + ' WHERE '
        cmd = self._cmd_add_values(cmd, identifiers, delimeter=delimeter)
        self._execute(cmd, commit=True)
        return self.cursor.lastrowid
