#!/usr/bin/env python

#NOTE: This module currently relies on Casen Hunger's QEMU system utilties to
#      be installed. Please contact casen@privasera.com for the code.

from python_utils.vm import VirtualMachineManagerBase
from python_utils.run import run_no_out


class QEMUManager(VirtualMachineManagerBase):

    def __init__(self):
        self.vm_list = []
        self.vm_map = {}
        return

    def get_vms(self):
        """ Create a list of VMs and their current MAC address, IPV4, and state. """
        self.vm_list = []
        self.vm_map = {}
    
        # First 2 lines of the output are the table headers
        output = [line for line in run(['qemu-list-vms']).split('\n')[2:] if line != '']

        for line in output:
            vm = line.split('\t')
            vm_info = {
                'name': vm[0],
                'MAC': None if vm[1] == 'None' else vm[1],
                'IPv4': None if vm[2] == 'None' else vm[2],
                'state': vm[3],
            }
            self.vm_list.append(vm_info)
            self.vm_map[vm[0]] = vm_info
        return self.vm_list

    def get_state(self, vm_name):
        """ 
            Return the state of a virtual machine.

            @param vm_name: Name of the virtual machine.
        """
        #TODO: Should throw an exception if now found?
        self.get_vms()
        if vm_name in self.vm_map:
            return self.vm_map[vm_name]['state']
        else:
            return None

    def get_ipv4(self, vm_name):
        """
            Return the IPv4 Address of the VM.

            @param vm_name: Name of the virtual machine.
        """
        self.get_vms()
        if vm_name in self.vm_map:
            return self.vm_map[vm_name]['IPv4']
        else:
            return None

    def clone_vm(self, src_vm_name, dest_vm_name):
        run(['qemu-clone-vm', src_vm_name, dest_vm_name])
        return

    def start_vm(self, vm_name):
        print("Before Start")
        run_no_out(['qemu-start-vm', vm_name], async_=True)
        print("After Start")
        return

    def stop_vm(self, vm_name):
        pass
    
    def create_vm(self, vm_name):
        raise NotImplementedError
    
    def delete_vm(self, vm_name):
        raise NotImplementedError
    
    def copy_vm(self, src_vm, dest_vm):
        raise NotImplementedError
