import boto3

ec2_client = boto3.client('ec2', region_name='us-west-2')
#instances = ec2_client.describe_instances()
#print instances
OWNER_ID = '868150887705'
images = ec2_client.describe_images(Filters=[
        {
            'Name': 'owner-id',
            'Values': [OWNER_ID] 
        }
    ])
print images


"""
for key, value in instances.iteritems():
    #print 'key' + key 
    #print value
    for i in instances[key]:
        if isinstance(i, dict):
            for key2, value2 in i.iteritems():
                if isinstance(value2, list):
                    print key2
                    for l in value2:
                        for key3, value3 in l.iteritems():
                            #print '\t' + key3 + ': ' + value3
                            print '\t{0}: {1}'.format(key3, value3)
                            #print ''
                else:
                    print key2 + ': ' + value2
            #print i.iteritems()
        else:
            print i
"""

"""
ec2_resource = boto3.resource('ec2', region_name='us-west-2')

KEY_NAME = 'TestKey'

outfile = open('TestKey.pem','w')
try:
    key_pair = ec2_resource.create_key_pair(KeyName=KEY_NAME)
    print(key_pair)
    outfile.write(str(key_pair.key_material))
except Exception as e:
    pass

# Change to custom image that has resource manager in init.d
UBUNTU_IMAGE = 'ami-b7a114d7'
INSTANCE_TYPE = 't2.medium'
BUBBLES_CASEN = 'sg-21e30946'
DEVICE_NAME = '/dev/sda1'
#security_group = ec2_resource.SecurityGroup(BUBBLES_CASEN)
#print(security_group)

instances = ec2_resource.create_instances(
    ImageId = UBUNTU_IMAGE,
    MinCount = 1,
    MaxCount = 1,
    KeyName = KEY_NAME,
    SecurityGroupIds = [BUBBLES_CASEN, ],
    InstanceType = INSTANCE_TYPE,
    BlockDeviceMappings = [
        {
            'DeviceName': DEVICE_NAME, 
            'Ebs': {
                'VolumeSize': 32,
                'DeleteOnTermination': True,
                },
        }, ])

instance_id = ''
for i in instances:
    print(i.id, i.instance_type)
    instance_id = i.id

instance = ec2_resource.Instance(instance_id)
print(instance)

print('before wait')
instance.wait_until_running()
print('after wait')

instance.create_tags(Tags = [{
    'Key': 'Name',
    'Value': 'Test-VM'
    }, ])
"""
