
from python_utils.vm import VirtualMachineManagerBase
from python_utils.run import run

import re
import boto3

class EC2Manager(VirtualMachineManagerBase):

    def __init__(self):
        self.vm_list = []
        self.vm_map = {}
        self.image_list = []
        self.image_map = {}
        # Must be using maching with AWS credentials installed
        # TODO: Get service and region arguments from config file or as arguments to constructor
        # Used to see information about existing VMs
        self.vm_client = boto3.client('ec2', region_name='us-west-2') 
        # Used to manipulate VMs 
        self.vm_resource = boto3.resource('ec2', region_name='us-west-2')
        self.vm_key_name = EC2Manager.get_key_pair()

    @classmethod
    def get_key_pair(cls):
        """ If key does not exist in directory, create a randomly named key pair """
        
        ls = run(['ls', '/var/www/blox/blox-core/python_utils/vm/'])
        keys = [key for key in ls.split() if re.match('.*.pem', key)]
        #print keys

        if len(keys) != 0:
            #print keys[0][:-4]
            return keys[0][:-4]

        else:
            # TODO: Create a randomly named key
            return 'TestKey'


    def get_vms(self):
        """ Create a list of VMs and their current MAC address, IPV4, instance_id, 
            and state. 
        """

        # Clear current list and map to make sure it is up to date
        self.vm_list = []
        self.vm_map = {}
        
        # Get response of all instances
        instances_res = self.vm_client.describe_instances()

        if instances_res['ResponseMetadata']['HTTPStatusCode'] != 200:
            return
        
        # Have not tested for different instances/ type of instances
        reservations = instances_res['Reservations']
        for reservation in reservations:
            for instance in reservation['Instances']:
                vm_info = {}
                vm_info['name'] = [tag['Value'] for tag in instance['Tags'] \
                                  if tag['Key'] == 'Name'][0]
                vm_info['instance_id'] = instance['InstanceId'] 
                vm_info['state'] = instance['State']['Name']

                if vm_info['state'] == 'running':
                    vm_info['MAC'] = instance['NetworkInterfaces'][0]['MacAddress'] 
                    vm_info['public_IPv4'] = instance['NetworkInterfaces'][0] \
                                                     ['Association']['PublicIp']
                    vm_info['private_IPv4'] = instance['NetworkInterfaces'][0] \
                                                      ['PrivateIpAddress'] 
                else:
                    vm_info['MAC'] = None
                    vm_info['public_IPv4'] = None
                    vm_info['private_IPv4'] = None

                self.vm_list.append(vm_info)
                self.vm_map[vm_info['name']] = vm_info

        #for l in self.vm_list:
        #    print l
        #print(self.vm_map)

        return self.vm_list

    def get_images(self):
        """
            Create a list and map of images and their names and image_ids
        """

        self.image_list = []
        self.image_map = {}

        #TODO: Read in owner_ID from arg or from config
        OWNER_ID = '868150887705'
        images_res = self.vm_client.describe_images(Filters=[
                {
                    'Name': 'owner-id',
                    'Values': [OWNER_ID] 
                }
            ])

        if images_res['ResponseMetadata']['HTTPStatusCode'] != 200:
            return

        images = images_res['Images']
        for image in images:
            image_info = {}
            image_info['name'] = image['Name']
            image_info['image_id'] = image['ImageId']

            self.image_list.append(image_info)
            self.image_map[image_info['name']] = image_info

        for i in self.image_list:
            print i

        return self.image_list


    def create_vm(self, vm_name, kwargs={}):
        """ Checks whether VM with vm_name already exists
            Checks whether kwargs has correct keys in dict
            Creates instance with arguments
            Adds vm to vm_list and vm_map

            @param vm_name: Name of the Virtual Machine
            @param kwargs: dict of arguments needed to create an EC2 Instance
                Optional kwargs:
                ubuntu_image - image of virtual machine to use 
                instance_type - size of ec2 instance 
                device_name - 
                security_group - list of security groups VM should be added to 
                volume_size - amount of disk space to be allocated (in GB)
        """

        # update vm_list
        self.get_vms()

        if vm_name in self.vm_map:
            #TODO: raise error
            pass

        if kwargs is None:
            #TODO: raise error
            pass
        
        args = {}
        #TODO: load default values from config file?
        # Load default values
        args['image_id'] = 'ami-c9d950a9'
        args['instance_type'] = 't2.medium'
        args['security_groups'] = ['sg-21e30946']
        args['device_name'] = '/dev/sda1'
        args['volume_size'] = 32

        # Change any args entered by user
        for key in args:
            if key in kwargs:
                args[key] = kwargs[key]

        instances = self.vm_resource.create_instances(
                ImageId = args['image_id'],
                MinCount = 1,
                MaxCount = 1,
                KeyName = self.vm_key_name,
                SecurityGroupIds = args['security_groups'],
                InstanceType = args['instance_type'],
                BlockDeviceMappings = [
                    {
                        'DeviceName': args['device_name'],
                        'Ebs': {
                            'VolumeSize': args['volume_size'],
                            'DeleteOnTermination': True,
                            },
                    }, ])

        instance_id = instances[0].id 
        instance = self.vm_resource.Instance(instance_id)
        instance.wait_until_running()
        # Give instance a name
        instance.create_tags(Tags = [{
            'Key': 'Name',
            'Value': vm_name
            }, ])

        vm_info = self.update_vm_info(vm_name, instance) 
        print vm_info

        return vm_info

    def change_name(self, instance_id, name):
        """
            Changes the name of the vm to name

            param:
            @instance_id: the ec2 instance id given by AWS
            @name: desired name of instance
        """
        instance = self.vm_resource.Instance(instance_id)
        instance.create_tags(Tags = [{
            'Key': 'Name',
            'Value': name
            }, ])
        vm_info = self.update_vm_info(name, instance)
        return

    def delete_vm(self, vm_name, instance_id=None):
        """
            Terminate the VM corresponding to vm_name

            @param vm_name: Name of the Virtual Machine 
            @param instance_id: EC2 instance id of VM given by AWS
                    (not to be confused with Resource Node attribute instanceId)
        """

        if instance_id is not None:
            instance = self.vm_resource.Instance(instance_id)
            instance.terminate()

            instance.wait_until_terminated()
            self.get_vms()
            return


        self.get_vms()

        if vm_name not in self.vm_map or self.vm_map[vm_name]['state'] is 'terminated':
            print('delete return')
            return

        instance = self.vm_resource.Instance(self.vm_map[vm_name]['instance_id'])
        instance.terminate()

        instance.wait_until_terminated()

        self.vm_list.remove(self.vm_map[vm_name])
        del self.vm_map[vm_name]
        return


    def copy_vm(self, src_vm, dest_vm):
        pass

    def get_state(self, vm_name):
        """ 
            Return the state of a Virtual Machine.

            @param vm_name: Name of the Virtual Machine.
        """
        self.get_vms()
        
        if vm_name in self.vm_map:
            return self.vm_map[vm_name]['state']
        return None

    def clone_vm(self, src_vm_name, dest_vm_name, kwargs={}):
        """ 
            Return the state of a Virtual Machine.

            @param src_vm_name: Name of the Virtual Machine you want to clone.
            @param dest_vm_name: Name of the Virtual Machine you want to create.
            @param kwargs: dict of arguments regarding cloneing the VM
                - name: Name given to image
                - noreboot: By default, Amazon shutsdown the src instance before
                  creating the image. You can set noreboot to true to make Amazon
                  not shutdown the instance. However, file system integrity on the
                  created image and dest_vm won't be guaranteed.
        """
        
        self.get_vms()
        self.get_images()
        if src_vm_name not in self.vm_map:
            print('copy return')
            #return

        image = None
        #TODO: if image exists, should i deregister the image and make a new one?
        if src_vm_name in self.image_map:
            image = self.vm_resource.Image(self.image_map[src_vm_name]['image_id'])

            while image.state != 'available':
                time.sleep(2)
                image = self.vm_resource.Image(self.image_map[src_vm_name]['image_id'])

        else:
            args = {
                'name': src_vm_name,
                'noreboot': False
            }

            # Change any args entered by user
            for key in args:
                if key in kwargs:
                    args[key] = kwargs[key]

            # Create an image of src_vm
            instance = self.vm_resource.Instance(self.vm_map[src_vm_name]['instance_id'])
            image = instance.create_image(
                Name = args['name'],
                NoReboot = args['noreboot'])

            image.wait_until_exists()

        # Create a new instance using image_id of src_vm 
        create_vm_args = {
            'image_id': image.image_id
        }
        self.create_vm(dest_vm_name, create_vm_args)
        return

    def start_vm(self, vm_name):
        """
            Start the VM corresponding to vm_name

            @param vm_name: Name of the Virtual Machine 
        """

        self.get_vms()

        if vm_name not in self.vm_map or self.vm_map[vm_name]['state'] is 'running':
            print('start return')
            return

        instance = self.vm_resource.Instance(self.vm_map[vm_name]['instance_id'])
        instance.start()

        instance.wait_until_running()

        self.vm_list.remove(self.vm_map[vm_name])
        vm_info = self.update_vm_info(vm_name, instance) 
        return


    def stop_vm(self, vm_name):
        """
            Stop the VM corresponding to vm_name

            @param vm_name: Name of the Virtual Machine 
        """
        self.get_vms()

        if vm_name not in self.vm_map or self.vm_map[vm_name]['state'] is 'stopped' \
                                      or self.vm_map[vm_name]['state'] is 'shutting-down':
            print('stop return')
            return

        instance = self.vm_resource.Instance(self.vm_map[vm_name]['instance_id'])
        instance.stop()

        instance.wait_until_stopped()

        self.vm_list.remove(self.vm_map[vm_name])

        vm_info = self.update_vm_info(vm_name, instance) 
        return

    def update_vm_info(self, vm_name, instance):
        """
            Returns a dict containing info about a VM from instance attributes

            @param vm_name: Name of the Virtual Machine 
            @param instance: an EC2 instance object
        """

        vm_info = {}
        vm_info['name'] = vm_name
        vm_info['instance_id'] = instance.instance_id
        vm_info['state'] = instance.state['Name']
        vm_info['MAC'] = instance.network_interfaces_attribute[0]['MacAddress'] 
        vm_info['public_IPv4'] = instance.public_ip_address 
        vm_info['private_IPv4'] = instance.private_ip_address

        self.vm_list.append(vm_info)
        self.vm_map[vm_info['name']] = vm_info

        return vm_info
