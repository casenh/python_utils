
import boto3
from python_utils.http import Request

"""
    Code for AWS Cognito Client that will send a request to the Developer 
    Provider and get an Open ID Token. The client will use the token to 
    contact AWS Cognito and get Credentials for Identity which give it 
    temporary AWS credentials to access AWS services (ex. S3)
    
    Image of Authentication Flow:
    http://docs.aws.amazon.com/cognito/latest/developerguide/images/amazon-cognito-dev-auth-enhanced-flow.png
"""

# TODO: add args for authentication in Developer Provider
def get_temp_aws_key(resource_manager_url, login_args):
    """
        Returns
        'IdentityId': 'string'
        'Credentials': {
            'AccessKeyId': 'string'
            'SecretKey': 'string'
            'SessionToken': 'string'
            'Expiration': datetime
            }
    """

    try:
        print(resource_manager_url)
        dp_req = Request(resource_manager_url)
        dp_args = {}
        dp_res = dp_req.make('post', '/url', {'data': dp_args})
    except Exception as e:
        raise e

    if dp_res['Result'] == 'Failure':
        return None

    # TODO: get service and region arguments from config file or as args to method
    ec2_client = boto3('cognito-identity', region_name='us-west-2')

    PROVIDER_NAME='cognito-identity.amazonaws.com'

    credentials = ec2_client.get_credentials_for_identity(
        IdentityId=dp_res['open_id']['IdentityId'],
        Logins={
            PROVIDER_NAME: dp_res['open_id']['Token'] 
            }
        )

    return credentials
    


