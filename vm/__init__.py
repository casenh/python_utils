#!/usr/bin/env python


class VirtualMachineManagerBase():
    """ 
        This class just defines the interface that all modules 
        should provide for virtual machine management. 
    """

    def __init__(self):
        return

    def get_vms():
        raise NotImplementedError
    
    def create_vm(vm_name):
        raise NotImplementedError
    
    def delete_vm(vm_name):
        raise NotImplementedError
    
    def copy_vm(src_vm, dest_vm):
        raise NotImplementedError
