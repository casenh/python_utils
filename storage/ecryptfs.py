#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


import errno
import os 
import subprocess
import time

from python_utils import create_id
from python_utils.storage.fs import FS
from python_utils.run import run, run_no_out


class EcryptFS(FS):

    @classmethod
    def initialize(cls, base_dir, passphrase):
        cls.base_dir = base_dir

        # Make sure we're not already mounted
        output = run(['mount'])
        for line in output.split('\n'):
            if base_dir in line and 'ecryptfs' in line:
                return

        # XXX: Create a temporary passphrase file that ecryptfs will
        # read from. This will prevent us from having to input it over the cmd line
        # which is readable from "ps". Append a large random number to the
        # file to prevent it from being intercepted before we delete it.
        filename = '/tmp/{0}'.format(create_id(64))
        with open(filename, 'w') as f:
            run(['chmod', '400', filename])
            f.write('passphrase_passwd={0}'.format(passphrase))

        params = 'passphrase_passwd_file={0}'.format(filename)
        params = '{0},ecryptfs_cipher=aes'.format(params)
        params = '{0},ecryptfs_key_bytes=32'.format(params)

        #TODO: Enable filename encryption
        params = '{0},ecryptfs_enable_filename_crypto=n'.format(params)
        params = '{0},ecryptfs_passthrough=n'.format(params)
        params = '{0},no_sig_cache'.format(params)
        ecrypt_p = run_no_out(['mount', '-t', 'ecryptfs', base_dir, base_dir, '-o', params], 
                       async_=False, sudo=True)
        os.remove(filename)
        return
   
    def __init__(self):
        pass

    @classmethod
    def teardown(cls):
        run_no_out(['umount', cls.base_dir], sudo=True)
        return
