#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = "Casen Hunger"
__copyright__ = "Copyright 2015, Casen Hunger"


import errno
import os 

from abc import ABCMeta, abstractmethod
from python_utils.run import run

STORAGE_PREFIX = '/var/foam'
APP_STORAGE = 'apps'
DATA_STORAGE = 'data'
SETTINGS_STORAGE = 'settings'


class StorageInterface:
    """
        This abstract class handles different types of operations for various 
        backend storage mechanisms.
            - Local Filesystem
            - S3
            - Dropbox #TODO
            - Google Drive #TODO
    """
    @classmethod
    def initialize(cls):
        """ 
            All subclasses must override this method and provide their own custom 
            arguments for initialization. This is where authentication should take 
            place, etc.
        """
        raise NotImplementedError

    def __init__(self, user_token):
        """ 
            Create a StorageInterface instance. Should accept a user_token argument in case
            it requires user authentication. 
           
            @param user_token: Identifier representing user.
        """
        raise NotImplementedError


    def create_folder(self, path):
        """
            Create a folder.

            @param path: The location where the folder should be created.
        """
        raise NotImplementedError


    def folder_exists(self, path):
        """
            Check if a folder exists.

            @param path: Path to the folder.

            @returns: True/False if the folder exists.
        """
        raise NotImplementedError


    def delete_folder(self, path):
        """
            Delete a folder.

            @param path: Location where the folder is located.
        """
        raise NotImplementedError

    
    def copy_folder(self, src_path, dest_path):
        """
            Copy a folder from one location to another.

            @src_path: Source folder to make a copy of.
            @dest_path: Destination location to save the copy to.
        """
        raise NotImplementedError

    
    def upload_file(self, file_src, file_dest, filename=None, content_type=None):
        """
            Upload a file to a given path.

            @param file_src: Location where the original file is currently saved.
            @param file_dest: Destination location where the file should be saved.
            @param filename: Name of the file.
            @param content_type: Content-Type of the file.
        """
        raise NotImplementedError


    def file_exists(self, path):
        """
            Check if a given file exists.

            @param path: Path to the file to check.

            @returns: True/False if the file exists.
        """
        raise NotImplementedError


    def delete_file(self, path):
        """
            Delete a file.

            @param path: Location where the file is currently saved.
        """
        raise NotImplementedError


    def download_file(self, src_path, dest_path):
        """
            Download a file to a source location.

            @param src_path: Source location of the file.
            @param dest_path: Destination where to save the file.
        """
        raise NotImplementedError


    def get_file(self, path):
        """
            Return a file descriptor object for a file.

            @param path: Path of the file.
        """
        #XXX: Not sure that we want this one. Don't know what they
        #     are planning to do with it, don't know how to open it.
        raise NotImplementedError
