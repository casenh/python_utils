#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


import errno
import os 

from python_utils.storage import StorageInterface
from python_utils.run import run, run_no_out

STORAGE_PREFIX = '/var/foam'
APP_STORAGE = 'apps'
DATA_STORAGE = 'data'
SETTINGS_STORAGE = 'settings'


class FS(StorageInterface):

    @classmethod
    def initialize(cls):
        return
   
    def __init__(self):
        pass

    def get(self, storage_type, user_id, container_id):
        user_id = str(user_id)
        container_id = str(container_id)
        if storage_type == 'application':
            path = os.sep.join([STORAGE_PREFIX, APP_STORAGE, container_id]) 
        elif storage_type == 'data':
            path = os.sep.join([STORAGE_PREFIX, user_id, DATA_STORAGE, container_id])
        elif storage_type == 'session':
            path = os.sep.join([STORAGE_PREFIX, user_id, SETTINGS_STORAGE, container_id])
        else:
            path = ''
        return '%s/' % (path)

    def create_folder(self, path):
        try:
            os.makedirs(path)
            run(['chown', 'casen:casen', path]) #TODO: Don't hardcode user
        except OSError as e:
            if e.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
        return

    def share_folder(self, path, emails):
        return {'LaunchURL': ''}

    def delete_folder(self, path):
        try:
            return run(['rm', '-r', path])
        except Exception as e:
            print(e)
            return

    def upload_file(self, src_path, dest_path):
        return run_no_out(['cp', src_path, dest_path])

    def delete_file(self, filename, path):
        return os.remove('%s/%s' % (path, filename))

    def exists(self, path):
        return os.path.isfile(path)

    def mount(self, mount_type, src, dest):
        if dest is not None:
            return run(['mount', '-o', mount_type, src, dest])
        else:
            return run(['mount', '-o', mount_type, src])
   
    def umount(self, dest):
        return run(['umount', dest])

