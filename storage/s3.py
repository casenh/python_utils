#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


import boto3, botocore
import errno
import os 
import tempfile

from python_utils.storage import StorageInterface
from python_utils.run import run


class S3(StorageInterface):
    #TODO: Need to add logging

    @classmethod
    def initialize(cls, bucket, access_key=None, secret_access_key=None, expires=100, path=None):
        cls.expires = expires
        cls.path = path
        cls.bucket = bucket
        cls.s3 = boto3.resource('s3')
        cls.client = boto3.client('s3',
                                  aws_access_key_id=access_key,
                                  aws_secret_access_key=secret_access_key)
        return

    def __init__(self, access_token):
        self.access_token = access_token
        return

    def append_path(self, path):
        if self.path is not None:
            dest_path = '{0}{1}'.format(self.path, path)
        else:
            dest_path = path
        return dest_path

    def share_folder(self):
        pass

    def get(self, storage_type, user_id, container_id):
        return

    def get_download_url(self, path, content_disposition=None):
        """ Generate a download URL for the file.

            @param path: Path to the file.
        """
        key = self.append_path(path)
        params = {
            'Bucket': self.bucket,
            'Key': key,
        }
        if content_disposition is not None:
            params['ResponseContentDisposition'] = content_disposition
        return self.client.generate_presigned_url('get_object', Params=params,
                                                  ExpiresIn=self.expires)
        

    def download_raw_file(self, path, file_obj):
        """
            Return a file object for the given file path.

            @param path: Path of the file.
        """
        key = self.append_path(path)
        return self.client.download_fileobj(self.bucket, key, file_obj)


    def download_file(self, src_path, dest_path):
        """
            Return a file object for a given file.

            @param path: Path to the file.
        """
        key = self.append_path(src_path)
        self.client.download_file(self.bucket, key, dest_path)
        return

    def create_folder(self, path):
        """ S3 has no notions of folder, just key + value. """
        pass

    def delete_folder(self, path):
        """
            Delete all of the objects in a folder on S3. S3 does not have folders directly,
            so we first need to iterate through all objects with a given prefix in their key.

            @param path: The path of the folder to delete.
        """
        dest_path = self.append_path(path)
        key_list = []
        for obj in self.client.list_objects(Bucket=self.bucket, Prefix=dest_path):
            key_list.append({'Key': obj.key})
        objects = { 'Objects': key_list }
        try:
            return self.client.delete_objects(Bucket=self.bucket, Delete=objects)

        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == 'MalformedXML':
                # Indicates that nothing was uploaded to the folder
                return
            else:
                raise e

    def copy_folder(self, src_path, dest_path):
        """ 
            Download a folder to a destination path.

            @param path: The source path to make a copy of.
            @param dest_path: Destination path to copy the conents to.
        """
        key = self.append_path(src_path)
        for obj in self.client.list_objects(Bucket=self.bucket, Prefix=dest_path):
            filename = obj.key.split('/')[-1]
            file_dest = '{0}/{1}'.format(dest_path, filename)
            self.client.download_file(self.bucket, obj.key, file_dest)
        return


    def upload_file(self, file_path, path, filename=None, content_type=None, file_descriptor=None):
        """
            Upload a file to a given S3 Bucket.

            @param file_object: The file object representing the file to upload.
            @param path: The path where the file should be uploaded to. 
            @param filename: The name that the file will be saved as when downloaded.
                             Set as the Content-Disposition of the file.
        """
        kwargs = {}
        if filename is not None:
            kwargs['ContentDisposition'] = filename
        if content_type is not None:
            kwargs['ContentType'] = content_type

        key = self.append_path(path)
        if file_descriptor is None:
            file_descriptor = open(file_path, 'r')
            self.client.put_object(Bucket=self.bucket, ACL='private', 
                                   Key=key, Body=file_descriptor, **kwargs)
            file_descriptor.close()

        else:
            self.client.put_object(Bucket=self.bucket, ACL='private', 
                                   Key=key, Body=file_descriptor, **kwargs)
        return 

    def delete_file(self, path):
        """
            Delete a file from a given S3 Bucket.

            @param path: The path for the file that should be deleted.
        """
        key = self.append_path(path)
        objects = { 'Objects': [{'Key': key}] }
        return self.client.delete_objects(Bucket=self.bucket, Delete=objects)

    
    def get_file(self, path, filename):
        """
            Return a file object for a given file.

            @param path: Path to the file.
        """
        key = self.append_path(path)
        tmp_file = tempfile.TemporaryFile('wb')
        self.client.download_fileobj(self.bucket, key, tmp_file)
        return tmp_file
         
    def create(self, path):
        pass 

    def exists(self, path):
        return os.path.isfile(path)

    def mount(self, mount_type, src, dest):
        return
   
    def umount(self, dest):
        return

    @classmethod
    def reset(cls):
        return
