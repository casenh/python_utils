#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


import dropbox
import errno
import os 

from python_utils.storage import StorageInterface
from python_utils.run import run

STORAGE_PREFIX = '/Blox/'

class Dropbox(StorageInterface):

    def __init__(self, access_token):
        self.dbx = dropbox.client.DropboxClient(access_token)

    def get(self, storage_type, user_id, container_id):
        pass

    def create_folder(self, path):
        return self.dbx.file_create_folder(path)

    def share_folder(self, path, emails):
        res = self.dbx.share(path)
        return {'LaunchURL': res['url'], 'Expires': res['expires']}
        
    def delete_folder(self, path):
        return self.dbx.file_delete(path)

    def upload_file(self, src_path, dest_path):
        file_ = open(src_path, 'rb')
        return self.dbx.put_file(dest_path, file_, overwrite=True)

    def delete_file(self, filename, path):
        return self.dbx.file_delete('%s/%s' % (path, filename))
         
    def create(self, path):
        pass 

    def exists(self, path):
        return os.path.isfile(path)

    def mount(self, mount_type, src, dest):
        return
   
    def umount(self, dest):
        return

    def delete(self, path):
        return run('rm', '-r', path)
