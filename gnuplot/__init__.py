#!/usr/bin/env python

__author__ = 'Casen Hunger'

import os
import subprocess


def create_graph(title, x_label, y_label, data, png_path, name_list=None, results_path=None):
    """ 
        Create a PNG graph using GNUPlot.

        @param title: Title of the graph.
        @param x_label: X-axis label.
        @param y_label: Y-axis label.
        @param data: A list of lists of 2 dimensional tuples -> [[(x1,y1),(x2,y2)...], [(x1,y1),(x2,y2)...],...].
        @param output_path: Path to save the PNG image.
        @param results_path: Path to save the data used to generate the graph.
    """
    # The command to run to create the plot
    make_pdf = \
"""#!/usr/bin/gnuplot
reset
set terminal png
set xlabel "{0}"
set ylabel "{1}"
set title "{2}"
set grid
set term png size 1400, 600
set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 ps 0.25    #--- blue
set style line 2 lc rgb '#dd181f' lt 1 lw 2 pt 5 ps 0.25    # --- red

""".format(x_label, y_label, title)

    if name_list is not None and len(name_list) != len(data):
        print("Invalid number of line titles. Make sure there is a title for each line plot.")
        name_list = None

    # Save the results file we'll use to graph
    for i in range(0, len(data)):
        data_path = results_path if results_path is not None else '/tmp/results{0}.txt'.format(i)
        with open(data_path, 'w') as f:
            for data_point in data[i]:
                f.write('{0}\t{1}\n'.format(data_point[0], data_point[1]))

        # Update the command with the data
        if i > 0:
            make_pdf = '{0},"{1}" with linespoints'.format(make_pdf, data_path)
        else:
            make_pdf = '{0}\nplot "{1}" with linespoints ls 1'.format(make_pdf, data_path)

        if name_list is not None:
            make_pdf = '{0} title "{1}"'.format(make_pdf, name_list[i])

    # Create a gnuplot script to create a PNG of the results
    gnu_cmd = '/tmp/make_pdf.sh'
    with open(gnu_cmd, 'w') as f:
        f.write(make_pdf)

    subprocess.call(['chmod', '+x', gnu_cmd])
    try:
        out = subprocess.check_output([gnu_cmd])
        with open(png_path, 'w') as f:
            f.write(out)

    except Exception as e:
        print(e)
        pass

    finally:
        os.remove(gnu_cmd)
    return


def convert_to_pdf(image_list, pdf_path):
    """
        Convert a list of images produced by GNUPlot into a single PDF.

        @param image_list: List of paths to image files.
        @param results_path: Where the resulting PDF should be saved.
    """
    pdf_cmd = ['convert']
    pdf_cmd += image_list
    pdf_cmd.append(pdf_path)
    subprocess.call(pdf_cmd)
    return
