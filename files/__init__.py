
import os, sys

class FileEditor:
    """ A class to help read and edit files. """

    def __init__(self, path, name=None):
        """
            Accept either path as including filename or path and name
            seperate parameters.
        """
        self.path = path
        self.name = name
        self.lines = self.read_file()
        return


    def __getitem__(self, key):
        """ Return a line with a given index. """
        return self.lines[key]

    def __len__(self):
        """ Return the number of lines in the file. """
        return len(self.lines)


    def IO_operation(self, func, mode='r+'):
        """ Performs function on the config file. """
        if self.name is None:
            config_name = self.path 
        else:
            config_name = os.path.join(self.path, self.name)
        with open(config_name, mode) as config:
            return func(config)

    def read_file(self):
        """ Reads a file and returns the file's lines """
        try:
            lines = self.IO_operation(lambda x: x.readlines())
            return lines
        except Exception as e:
            raise e


    def insert_line(self, index, line):
        """ Writes a line in the middle of the config file """
        self.lines.insert(index, line)
        self.IO_operation(lambda x: x.writelines(self.lines))
        return


    # TODO: Could also use insertLine for append
    def append_line(self, line):
        """ Writes a line to the end of the config file """
        self.lines.append(line)
        self.IO_operation(lambda x: x.writelines(self.lines))
        return


    def replace_line(self, index, line):
        """ Replaces a line in the config file """ 
        self.lines[index] = line
        self.IO_operation(lambda x: x.writelines(self.lines), mode='w')
        return

    def delete_lines_with(self, pattern):
        """
            Delete all lines with a certain pattern.

            @param pattern: Pattern to search for to delete.
        """
        new_lines = []
        for line in self.lines:
            if pattern not in line:
                new_lines.append(line)
        self.lines = new_lines
        return


    def delete_line(self, index):
        """ Deletes the line at the index from the config file """
        self.lines.pop(index)

        # NOTE: If we don't set the write mode here, we can get odd behavior
        # of incomplete file writing.
        self.IO_operation(lambda x: x.writelines(self.lines), mode='w')
        return


    def lines_containing(self, pattern, strict=False):
        """ 
            Returns indices for lines in the config file that match
            the pattern (empty list if no match found).
        """
        if strict is True:
            indices = filter(lambda i: pattern == self.lines[i], range(len(self.lines)))
        else:
            indices = filter(lambda i: pattern in self.lines[i], range(len(self.lines)))
        return indices 

