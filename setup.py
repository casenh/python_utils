#!/usr/bin/env python

# mysql-client, mysql-server, libmysqld-dev, libpython-dev


from setuptools import setup

setup(  name='bubbles_proxy'
        ,version='1.0'
        ,url='http://privasera.com'
        ,author='Casen Hunger'
        ,install_requires=[
            'pymongo'
            ,'MySQL-python'
            ,'dropbox'
            ,'boto3'
            ,'beaker'
        ]
    )
