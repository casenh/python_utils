#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'

import json
import os
import threading
from datetime import datetime


class Log():
    """ A Log class for easy logging of debug and error statements. """

    # Available logging debug levels
    LEVELS = {'debug': 0, 'info': 1, 'warn': 2, 'error': 3}

    def __init__(self, log_path, filename, level):
        """
            @param log_path: Path where the log file should be written.
            @param filename: Filename to save the log to.
            @param level: Log level to use. Anything logged below this will be ignored.
        """
        try:
            os.makedirs(log_path)
        except OSError as e:
            # File exists
            if e.errno != 17:
                raise e

        self.log_path = log_path 
        self.filename = filename
        self.file_path = '{0}/{1}.log'.format(log_path, filename)
        self.level = self.LEVELS[level]
        return
    
    def write(self, _level, _label, *args, **kwargs):
        """
            Write a log message. Will automatically include the time
            and thread ID as well.

            @param _level: The level the message should be written at.
            @param _label: Label for the message.
            @param *args: List of statements to print.
            @param **kwargs: Dict of objects and values to print.
        """
        if self.LEVELS[_level] < self.level:
            return

        output = {
            'Level': _level,
            'Label': _label,
            'Time': str(datetime.utcnow()),
            'ThreadID': str(threading.current_thread()),
        }

        # Print generic lines as "statement#" in a JSON object
        statement_count = 0
        for line in args:
            try: 
                output['statement{0}'.format(statement_count)] = str(line)
            except:
                try:
                    output['statement{0}'.format(statement_count)] = unicode(line, 'utf-8')
                except:
                    output['statement{0}'.format(statement_count)] = 'Unable to decode input' 
            statement_count += 1

        # JSON object will directly mimic the Dict
        for key, line in kwargs.iteritems():
            try:
                output[key] = str(line)
            except:
                try:
                    output[key] = unicode(kwargs[kw], 'utf-8')
                except:
                    output[key] = 'Unable to decode input'

        with open(self.file_path, 'a') as f:
            try:
                f.write('{0},\n'.format(json.dumps(output, indent=2, separators=(',',': '))))
            except IOError as e:
                # Disk could be full or a permission problem. Either way, let's not bring down the app.
                pass
    
        return


    def parse(self):
        """ Return a list of Dictionary objects for each log entry. """
        with open(self.file_path, 'r') as f:
            output = ''.join(f.readlines())
        output = '{0}]'.format(output[:-2])
        logs = json.loads(output)
        return logs
    
    
    def line(self, level, string):
        debug = getattr(self.logger, level)

    def clear(self):
        with open(self.file_path, 'w') as f:
            f.write('[')
