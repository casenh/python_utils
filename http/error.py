#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = "Casen Hunger"
__copyright__ = "Copyright 2015, Casen Hunger"

''' Various errors that can be thrown for the User model are defined here '''

class TimeoutError(Exception):
    def __init__(self, message=None):
        super(TimeoutError, self).__init__(message)
        return
