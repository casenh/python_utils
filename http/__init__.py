#!/usr/bin/env python

import requests
import random
import time
import base64

from python_utils.http.error import TimeoutError


class Request():

    #TODO: Add a logging feature here
    def __init__(self, url, certificate=None, use_session=False):
        self.url = url
        self.access_token = None
        self.certificate = certificate
        if use_session == True:
            self.session = requests.Session()
        else:
            self.session = None
        return

    def set_access_token(self, access_token):
        self.access_token = access_token

    def set_new_session(self):
        self.session = requests.session()

    @classmethod
    def disable_warnings(cls):
        requests.packages.urllib3.disable_warnings()

    def poll_request(self, req_type, url, args, callback, timeout):
        start = time.time()
        while True:
            res = self.make(req_type, url, args)
            if callback(res) == True:
                break

            if time.time() - start > timeout:
                raise TimeoutError
            time.sleep(1)
        return res


    def make(self, req_type, url, args={}, timeout=5):
        if 'headers' not in args: args['headers'] = None
        if 'data' not in args: args['data'] = None
        if 'json' not in args: args['json'] = None
        if 'files' not in args: args['files'] = None
        if 'auth' not in args: args['auth'] = None
        if 'params' not in args: args['params'] = None


        # Add in the access token if required
        if self.access_token is not None and args['headers'] is not None:
            args['headers']['accessToken'] = self.access_token
        elif self.access_token is not None and args['headers'] is None:
            args['headers'] = {'accessToken': self.access_token}

        try:
            # Use session if needed
            if self.session is not None:
                req = getattr(self.session, req_type)
            else:
                req = getattr(requests, req_type)
        except AttributeError as e:
            print('Incorrect request type. Please fix')

        # Add prefix if they did not provide one
        if 'http' not in url:
            url = '{}{}'.format(self.url, url)

        if self.certificate is not None:
            return req(url, headers=args['headers'], data=args['data'], 
                       json=args['json'], files=args['files'], auth=args['auth'],
                       params=args['params'], verify=False, timeout=timeout, 
                       cert=self.certificate)
        else:
            return req(url, headers=args['headers'], data=args['data'], 
                       json=args['json'], files=args['files'], auth=args['auth'],
                       params=args['params'], verify=False, timeout=timeout)
