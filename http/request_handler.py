#!/usr/bin/env python

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
from ssl import SSLError
from StringIO import StringIO
import sys
import urlparse


class HTTPRequestHandler(BaseHTTPRequestHandler):
    """ 
        A Class to Handle incoming HTTP requests from a BaseHTTPServer. Check out 
        /usr/lib/python2.7/SocketServer.py for further details.
    """

    @classmethod
    def set_handler(cls, request_handler):
        """ 
            Set a custom function to handle requests.
       
            @param request_handler: Function to call that handles an incoming request. Should take
                                    the following args:
                - Arg1: The instace of this class
        """
        cls.request_handler = request_handler
        return


    def __init__(self, connection, client_addr, server):
        """
            Data and JSON should be set to None to be compatible with the requests library.

            @param connection: Connection that the socket accepted
            @param client_addr: Client address received from socket connection
            @param server: Socket that we are listening on
            @param request_handler: Function pointer that handles the request
        """
        self.data = None
        self.json = None
        try:
            BaseHTTPRequestHandler.__init__(self, connection, client_addr, server)
        except SSLError as e:
            pass #TODO
        except IOError as e:
            print("IOError handled at the top")
        return

    def _get_attributes(self):
        """
            Set the attributes for the object given the most recent request. Should be called
            before handling the request.
        """
        if hasattr(self, 'headers'):
            self.headers = self.headers.dict
        else:
            self.headers = {}

        self.cookies = {}
        if 'cookie' in self.headers and self.headers['cookie'] != '':
            cookie_list = self.headers['cookie'].split(';')
            for cookie in cookie_list:
                split = cookie.split('=')
                if len(split) != 2:
                    continue
                self.cookies[split[0].strip()] = split[1].strip()

        # Need to identify the type of request so we can handle websockets if necessary
        if 'upgrade' in self.headers and self.headers['upgrade'].lower() == 'websocket':
            self.https = True
            self.address = 'wss://{0}{1}'.format(self.headers['host'], self.path)
        else:
            self.https = True
            self.address = 'https://{0}{1}'.format(self.headers['host'], self.path)

        self.url = urlparse.urlparse(self.address)

        # Grab querystring agruments
        self.args = {}
        for query_string in self.url.query.split('&'):
            query_split = query_string.split('=')
            if query_string == '' or len(query_split) != 2:
                break
            self.args[query_split[0]] = query_split[1]

        split_host = self.url.hostname.split('.')
        if len(split_host) > 2:
            self.subdomain = '.'.join(split_host[:-2])
        else:
            self.subdomain = None

        # Compatibility
        self.method = self.command 
        return


    def do_GET(self):
        """ Handle a GET request """
        self._get_attributes()
        self.request_handler()
        return


    def do_POST(self):
        """ Handle a POST request. Requires parsing out the data sent in with the POST. """

        self._get_attributes()
        #TODO: Watch for missing content-length
        self.data = self.rfile.read(int(self.headers['content-length']))
        self.request_handler()
        return


    def finish(self):
        """ 
            Finish the request. Make sure all data has been written to the socket and the connection
            is closed.
        """
        if not self.wfile.closed:
            try:
                self.wfile.flush()
            except Exception as e:
                pass

        try:
            self.wfile.close()
        except IOError as e:
            print("Got IOError!")
            print(e)

        try:
            self.rfile.close()
        except IOError as e:
            print("Got IOError!")
            print(e)
        
        try:
            self.request.close()
        except IOError as e:
            print("Got IOError!")
            print(e)
        return


    def get_json(self):
        """ Get the JSON data sent with the request """
        return self.json


    def get_data(self):
        """ Get the Form data sent with the request """
        return self.data


#    def send_error(self, code, message):
#        self.error_code = code
#        self.error_message = message
#        return
