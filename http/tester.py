#!/usr/bin/env python

from operator import itemgetter
import os
import string
import random
import subprocess
import sys
import time

from . import Request

class Tester():
    
    # Each object should be a dictionary
    # {
    #    url: /url
    #    attributes: [(name, type)]
    #    create: None or function to call to create new
    #    compare: None or "All"
    def __init__(self, url, log=None, delay=None, test_objects=[], auth_token=None, freq=[("get",25), ("put",25), ("post",25), ("delete",25)]):
        Request.disable_warnings()
        self.url = url
        self.request = Request(url, use_session=True)
        self.routes = test_objects
        self.test_objs = []
        self.auth_token = auth_token
        self.freq = freq
        self.times = {}
        self.delay = delay
        self.log = log
        self.username = None
        self.password = None
        return
   

    # TODO: Add check to make sure we get a valid access token
    def get_access_token(self, username="admin", password="pass"):
        self.username = username
        self.password = password

        # Log in the user and get the temporary token
        r = self.request.make("post", "/oauth/login", args={'json': {'email': username, 'password': password}})
        assert r.status_code == 200
        j = r.json()
    
        # Swap the temporary token for the access token
        r = self.request.make("post", "/oauth/token", args={"json": {'temporaryToken': j['TemporaryToken'], 'clientId': '123456789', 'clientSecret': '987654321'}})
        assert r.status_code == 200
        j = r.json()

        self.access_token = j['AccessToken']
        self.request.set_access_token(self.access_token)
        return j['AccessToken']


    def add_test_object(self, obj):
        self.routes.append(obj)

    # Dictionary of {"GET": freq, "POST": ...}. Freq out of 100
    def set_request_freq(self, freq):
        self.freq = freq


    def run_test(self, num_requests, log=None):
        for n in range(0, num_requests):
            # Select a URL based on specified frequencies
            if 'route_frequency' in self.routes[0]:
                rand = random.randint(0, 99)
                for route in self.routes:
                    if(route['route_frequency'] > rand):
                        URL = route
                        break
                    else:
                        rand -= route['route_frequency']
            # Select a random URL to test
            else:
                URL = self.routes[random.randint(0, len(self.routes) - 1)]

            # Pick the type of request to make based on specified request frequencies
            rand = random.randint(0, 99)
            frequency = self.freq if 'frequency' not in URL else URL['frequency']
            for req_type in frequency:

                # Compare the random number to the frequency for the request
                if(req_type[1] > rand):
                    request = getattr(self, req_type[0])
                    try:
                        request(URL)
                    except Exception as e:
                        if log is not None:
                            log.write('error', 'Tester.run_test - exception', e=e)
                        else:
                            raise e

                    if self.delay != None:
                        time.sleep(self.delay)
                    break
                else:
                    # Adjust the probabilty for the next test
                    rand -= req_type[1]

    def get(self, obj):
        try:
            (url, args) = obj["pre_get"](self, obj)
            if url is None:
                return
        except:
            url = obj["url"]
            args = {}

        response, error = self.time_request(obj, "get", url, args=args)

        try:
            obj["post_get"](self, response, error)
        except:
            pass
        return response

    def put(self, route):
        if len(self.test_objs) == 0:
            return
        #old_obj = self.test_objs[random.randint(0,len(self.test_objs) - 1)]
        
        try:
            (url, args) = route["pre_put"](self, route)
            if url is None:
                return

        except:
            return

        #for attribute in route["attributes"]:
        #    old_obj[attribute[0]] = create_random_value(attribute[1])

        response, error = self.time_request(route, "put", url, args=args)
        
        try:
            route["post_put"](self, response, error)
        except:
            pass
        return response


    def post(self, obj):
        new_obj = {}
        for attribute in obj["attributes"]:
            new_obj[attribute[0]] = create_random_value(attribute[1])

        try:
            url, args = obj["pre_post"](self, obj, new_obj)
            if url is None:
                return
        except Exception as e:
            print('pre-post error')
            print(e)
            url = obj["url"]
            args = {"json": new_obj}

        response, error = self.time_request(obj, "post", url, args=args)

        self.test_objs.append(new_obj)
        
        try:
            obj["post_post"](self, response, new_obj, error)
        except:
            pass

        return response

    def delete(self, obj):
        
        try:
            url, args = obj["pre_delete"](self, obj) 
            if url is None:
                return

        except Exception as e:
            print('pre-delete error')
            print(e)
            if len(self.test_objs) == 0:
                return

            old_obj = self.test_objs[random.randint(0,len(self.test_objs) - 1)]
            url = obj["url"]
            args = {"json": old_obj}

        response, error = self.time_request(obj, "delete", url, args)

        try:
            obj["post_delete"](self, response, error)
        except:
            pass
        return response

    def time_request(self, obj, method, url, args):
        response = None
        error = None
        try:
            start = time.time()
            response = self.request.make(method, url, args=args)
            end = time.time()

            key = obj['name']
            if key not in self.times:
                self.times[key] = {}
            if method not in self.times[key]:
                self.times[key][method] = [{ 'start': start, 'end': end }]
            else:
                self.times[key][method].append({'start': start, 'end': end})

        except Exception as e:
            if self.log is not None:
                self.log.write('error', 'Tester.time_request - exception'
                                , username=self.username, error=e
                                , obj=obj, method=method, url=url
                                , args=args)
            else:
                print('Time Request error')
                error = e
        
        return response, error


def write_results(path, testers, pdf_title):

    times = {}
    for tester in testers:

        for key, value in tester.times.iteritems():
            for method, results in value.iteritems():

                if key not in times:
                    times[key] = {}
                if method not in times[key]:
                    times[key][method] = results
                else:
                    times[key][method] += results
   
    image_files = []
    for key, value in times.iteritems():
        for method, results in value.iteritems():
         
            # Handle url's with multiple slashes
            #key = key.replace('/', '-')
            #key = '/%s' % (key[1:])
            file_path = '%s%s_%s.txt' % (path, key, method)
            with open(file_path, 'w') as f:
    
                index = 1
                sorted_results = sorted(results, key=itemgetter('start'))
                for result in results:
                    ellapsed = float(result['end']) - float(result['start'])
                    f.write('%s\t%s\n' % (index, ellapsed))
                    index += 1

            # Create a gnuplot script to create a PNG of the results
            gnu_cmd = '%s/make_pdf.sh' % (path)
            with open(gnu_cmd, 'w') as f:
                make_pdf = \
"""#!/usr/bin/gnuplot
reset
set terminal png
set ylabel "Time (s)"
set xlabel "Request Number"
set grid
set term png size 1400, 600
set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 ps 0.25    #--- blue
set style line 2 lc rgb '#dd181f' lt 1 lw 2 pt 5 ps 0.25    # --- red

plot "%s" with linespoints ls 1
""" % (file_path)
                f.write(make_pdf)
                
            subprocess.call(['chmod', '+x', gnu_cmd])
            out = subprocess.check_output([gnu_cmd])
            
            png_path = '%s%s_%s.png' % (path, key, method)
            image_files.append(png_path)
            with open(png_path, 'w') as f:
                f.write(out)
            os.remove(gnu_cmd)

    # Create a pdf of all images
    if len(image_files) > 0:
        pdf_cmd = ['convert', pdf_title]
        pdf_cmd += image_files
        pdf_cmd.append('%s/results.pdf' % (path))
        subprocess.call(pdf_cmd)

    return


def create_random_value(type):
    if type == "int":
        return random.randint(0,100)
    if type == "string":
        return rand_string()
    if type == "bool":
        return random.choice([True, False])


def rand_string():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))
