#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = "Casen Hunger"
__copyright__ = "Copyright 2015, Casen Hunger"

from flask import jsonify, make_response
from functools import wraps
import traceback


def exception_handler(func):
    """ Decorator for default handling of all errors for the webserver """
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = None
        try:
            response = func(*args, **kwargs)
        
        except Exception as e:
            if not hasattr(e, 'status_code') or not hasattr(e, 'error_code') \
                    or not hasattr(e, 'dev_msg') or not hasattr(e, 'usr_msg'):
                content = {
                    'ErrorCode': 800,
                    'UserMessage': 'An unknown error as occured',
                    'DeveloperMessage': 'An unknown error has occured'
                }
                status = 500

            else:
                content = {
                    'ErrorCode': e.error_code,
                    'UserMessage': e.usr_msg,
                    'DeveloperMessage': e.dev_msg,
                }
                status = e.status_code

            if BaseServerException._log is not None:
                log = BaseServerException._log
                log.write('error', 'exception_handler - exception', e=e,
                          response=response, trace=traceback.format_exc())

            response = make_response(jsonify(content), status)

        finally:
            return response
    return wrapper


class BaseServerException(Exception):
    """ The base exception class that all custom exceptions inherit from. """

    # Can set logging up for logging of all exceptions automatically
    _log = None

    @classmethod
    def initialize(cls, log):
        if log is not None:
            cls._log = log
        return


class AccessControlErrorBase(BaseServerException):
    """ Exception that should be raise for all invalid accesses to protected information. """

    def __init__(self, dev_msg, usr_msg, error_code):
        super(AccessControlErrorBase, self).__init__()
        if self._log is not None:
            self._log.write('error', 'AccessControlErrorBase - exception', error_code=error_code,
                dev_msg=dev_msg, usr_msg=usr_msg)

        self.status_code = 403
        self.error_code = error_code
        if dev_msg is not None:
            self.dev_msg = dev_msg
        else:
            self.dev_msg = 'Access control violation for the requested resource.'

        if usr_msg is not None:
            self.usr_msg = usr_msg
        else:
            self.usr_msg = 'Permission denied. Please try again.'
        return


class ArgumentErrorBase(BaseServerException):
    """ 
        Exception that should be raise when invalid arguments are passed to constructors,
        methods, etc.
    """
    def __init__(self, dev_msg, usr_msg, error_code):
        super(ArgumentErrorBase, self).__init__()
        if self._log is not None:
            self._log.write('error', 'ArgumentErrorBase - exception', error_code=error_code,
                dev_msg=dev_msg, usr_msg=usr_msg)

        self.status_code = 400
        self.error_code = error_code
        if dev_msg is not None:
            self.dev_msg = dev_msg
        else:
            self.dev_msg = 'The arguments passed to the request are invalid.'

        if usr_msg is not None:
            self.usr_msg = usr_msg
        else:
            self.usr_msg = 'Please check your syntax and try agin.'
        return


class AuthenticationErrorBase(BaseServerException):
    """ Exception that should be raise when a user has not be authenticated before making a request. """

    def __init__(self, dev_msg, usr_msg, error_code):
        super(AuthenticationErrorBase, self).__init__()
        if self._log is not None:
            self._log.write('error', 'AuthenticationErrorBase - exception', error_code=error_code,
                    dev_msg=dev_msg, usr_msg=usr_msg)

        self.status_code = 403
        self.error_code = error_code
        if dev_msg is not None:
            self.dev_msg = dev_msg
        else:
            self.dev_msg = 'Unable to authenticate the requested resource.'

        if usr_msg is not None:
            self.usr_msg = usr_msg
        else:
            self.usr_msg = 'Invalid credentials. Please try again.'
        return


class ExecutionErrorBase(BaseServerException):
    """ Exception that should be raise when execution within code fails. """

    def __init__(self, dev_msg, usr_msg, error_code):
        super(ExecutionErrorBase, self).__init__()
        if self._log is not None:
            self._log.write('error', 'ExecutionErrorBase - exception', error_code=error_code,
                            dev_msg=dev_msg, usr_msg=usr_msg)

        self.status_code = 500
        self.error_code = error_code
        if dev_msg is not None:
            self.dev_msg = dev_msg
        else:
            self.dev_msg = 'Access control violation for the requested resource.'

        if usr_msg is not None:
            self.usr_msg = usr_msg
        else:
            self.usr_msg = 'Permission denied. Please try again.'
        return


class NoSuchResourceBase(BaseServerException):
    """ Exception that should be raised when a resource (DB Model, etc.) cannot be found. """

    def __init__(self, dev_msg, usr_msg, error_code):
        super(NoSuchResourceBase, self).__init__()
        if self._log is not None:
            self._log.write('error', 'NoSuchResourceBase - exception', error_code=error_code,
                            dev_msg=dev_msg, usr_msg=usr_msg)

        self.status_code = 404
        self.error_code = error_code
        if dev_msg is not None:
            self.dev_msg = dev_msg
        else:
            self.dev_msg = 'Unable to find the requested resource.'

        if usr_msg is not None:
            self.usr_msg = usr_msg 
        else:
            self.usr_msg = 'Resource not found. Please try again.'
        return


class RequestErrorBase(BaseServerException):
    """ Exception that should be made when a remote request (HTTP, RPC) fails. """

    def __init__(self, dev_msg, usr_msg, error_code):
        super(RequestErrorBase, self).__init__()
        if self._log is not None:
            self._log.write('error', 'RequestErrorBase - exception', error_code=error_code,
                            dev_msg=dev_msg, usr_msg=usr_msg)

        self.status_code = 503
        self.error_code = error_code
        if dev_msg is not None:
            self.dev_msg = dev_msg
        else:
            self.dev_msg = 'Request for remote resource failed.'

        if usr_msg is not None:
            self.usr_msg = usr_msg
        else:
            self.usr_msg = 'Resource busy. Please try again.'
        return
