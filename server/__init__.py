#!/usr/bin/env python

from flask import Flask
import inspect
import os
import random
import socket
import time

from python_utils.http import Request
from python_utils.networking import apache
from python_utils.run import run, run_no_out
from python_utils.server.error import BaseServerException


class FlaskImproved(Flask):
    """
        A wrapper around the base Flask class that provides functionality for
        running behind Apache, provided that certain configuration variables
        are provided.
    """

    def __init__(self, webserver_config, *args, **kwargs):
        """
            @param webserver_config: Configuration class.
        """
        self._config = webserver_config
        super(FlaskImproved, self).__init__(*args, **kwargs)
        return


    def run_app(self):
        """
            Start the webserver either as a raw Flask app or behind
            Apache.
        """
        config = self._config

        if hasattr(self._config, 'DEBUG') and self._config.DEBUG is True:
            apache.remove_listen_port(config.PORT)
            apache.reload()
            self.run(host=self._config.SERVER_IP, port=self._config.PORT,
                    ssl_context=(self._config.HTTPS_CERT, self._config.HTTPS_KEY),
                    debug=False, threaded=True)
    
        else:
            apache.add_listen_port(self._config.PORT)
            apache.enable_site(self._config.WSGI_DIR_PATH, self._config.APACHE_CONF_NAME, 
                               service_name=self._config.SERVICE_NAME,
                               server_names=[self._config.URL, self._config.LOCAL_URL],
                               log_level=self._config.LOG_LEVEL.lower(),
                               error_path=self._config.LOG_PATH,
                               log_path=self._config.LOG_PATH,
                               wsgi_path=self._config.WSGI_PATH,
                               cert=(self._config.HTTPS_CERT, self._config.HTTPS_KEY))
            apache.reload()
            while True:
                time.sleep(60)
        return


    def teardown_app(self):
        """
            Teardown the application so we can gracefully stop. Stop Apache
            if necessary.
    
            @param config. Config object to use.
        """
        if hasattr(self._config, 'DEBUG') and self._config.DEBUG is False:
            apache.remove_listen_port(self._config.PORT)
            apache.disable_site(self._config.APACHE_CONF_NAME)
            apache.reload()
        return


# TODO: This should be moved into the FlaskServer application.
def configure_server(app_name, config):
    """ 
        A function for configuring a Flask server. Will initialize a database connection, log,
        cache connection, and Flask app.
   
        @param app_name: Name of your webserver instance
        @param config: A config class that should have the following attributes:
            - UPLOAD_PATH: Path where uploaded files will be stored
            - STATIC_PATH: Path where static files are served
            - TEMPLATE_PATH: Path where template files are served from
            - HTTPS: True/False whether to use SSL. If True, also provide:
                + HTTPS_CERT: Location to the certificate file
                + HTTPS_KEY: Location to the private key file
            - LOG_PATH: Path where to save the log file
            - LOG_LEVEL: The level to initialize the log file to
            - DATABASE_TYPE: Type of database to use ('mysql', 'mongo')
            - DATABASE_HOST: Location of the database (ex. 'localhost')
            - DATABASE_USERNAME: Username for the database connection
            - DATABASE_PASSWORD: Password for the database connection
            - CACHE_TYPE: Type of cache instance to create ('file', 'memory', 'redis')
            - CACHE_ARGS: Dictionary of arguments passed to the cache configuration
    """
    from python_utils.log import Log
    Request.disable_warnings()

    # Set the user:group for chown the created directories
    chown_user = None
    if hasattr(config, 'PROCESS_USER'):
        chown_user = config.PROCESS_USER
    if chown_user is not None and hasattr(config, 'PROCESS_GROUP'):
        chown_user = '{0}:{1}'.format(chown_user, config.PROCESS_GROUP)

    # Initialize the directories that our app will use
    def chown(user, path):
        if user is not None:
            try:
                run_no_out(['chown', '-R', chown_user, path], sudo=True)
            except Exception as e:
                pass

    if hasattr(config, 'UPLOAD_PATH'):
        try:
            os.makedirs(config.UPLOAD_PATH)
        except (OSError, AttributeError) as e:
            pass
        chown(chown_user, config.UPLOAD_PATH)

    if hasattr(config, 'RUN_PATH'):
        try:
            os.makedirs(config.RUN_PATH)
        except (OSError, AttributeError) as e:
            pass
        chown(chown_user, config.RUN_PATH)

    if hasattr(config, 'PID_PATH'):
        try:
            os.makedirs(config.PID_PATH)
        except (OSError, AttributeError) as e:
            pass
        chown(chown_user, config.PID_PATH)

    if hasattr(config, 'LIB_PATH'):
        try:
            os.makedirs(config.LIB_PATH)
        except (OSError, AttributeError) as e:
            pass
        chown(chown_user, config.LIB_PATH)

    if hasattr(config, 'LOG_PATH'):
        try:
            os.makedirs(config.LOG_PATH)
        except (OSError, AttributeError) as e:
            pass
        chown(chown_user, config.LOG_PATH)

    # Configure our logging setup and set up logging for exception handling.
    log = Log(config.LOG_PATH, config.LOG_NAME, config.LOG_LEVEL)
    log.clear()
    BaseServerException.initialize(log)

    # Configure our database
    if config.DATABASE_TYPE == 'mysql':
        from python_utils.database.mysql import MySQLConnection as DBConnection

    username = os.environ.get('DATABASE_USERNAME', config.DATABASE_USERNAME)
    password = os.environ.get('DATABASE_PASSWORD', config.DATABASE_PASSWORD)
    host = os.environ.get('DATABASE_HOST', config.DATABASE_HOST)
    DBConnection.init(username, password, host, logger=log)

    # Configure caching
    if config.CACHE_TYPE == 'file':
        from python_utils.cache.filecache import FileCache as Cache
    elif config.CACHE_TYPE == 'memory':
        from python_utils.cache.dictionarycache import DictionaryCache as Cache
    elif config.CACHE_TYPE == 'redis':
        from python_utils.cache.rediscache import RedisCache as Cache

    Cache.initialize(config.CACHE_ARGS)
    if config.CLEAR_CACHE == True:
        Cache.clear_cache()

    # Create the Flask instance. Use the FlaskImproved class for convenient
    # functions that wrap the base Flask app.
    app = FlaskImproved(config, app_name, 
                        static_folder=config.STATIC_PATH, 
                        template_folder=config.TEMPLATE_PATH)
    app.config.from_object(config)
    return app, log, DBConnection, Cache


def create_pid_file(pid_path, filename, pid=None):
    """
        Write a file with the PID of the current running process.
        TODO: If the file already exists, raise an exception.

        @param pid_path: Path to the directory of the pid file.
        @param filename: Name for the pid file.
        @param pid: Can pass in a custom PID if not writing the current process'.
    """
    if not os.path.isdir(pid_path):
        os.makedirs(pid_path)

    with open('{0}/{1}.pid'.format(pid_path, filename), 'w') as f:
        if pid is None:
            f.write('{0}'.format(os.getpid()))
        else:
            f.write('{0}'.format(pid))
    return

def get_pid(pid_path, filename):
    """
        Return the PID from a PID file.
        
        @param pid_path: Path to the directory of the pid file.
        @param filename: Name for the pid file.
    """
    try:
        with open('{0}/{1}.pid'.format(pid_path, filename), 'r') as f:
            return int(f.readline().strip())
    except IOError as e:
        return None
    return

def delete_pid_file(pid_path, filename):
    """
        Delete a PID file if it exists. Should be used to clean up a process.

        @param pid_path: Path to the directory of the pid file.
        @param filename: Name for the pid file.
    """
    file_path = '{0}/{1}.pid'.format(pid_path, filename)
    try:
        os.remove(file_path)
    except OSError as e:
        pass
    return


def get_open_port(port_range=None):
    """
        Get an open port to use

        @param port_range: A list of port numbers, where the first element is lower bound and 
                           second element is upper bound.
    """
    port = None
    sock = None
    if port_range is not None:
        if len(port_range) != 2 or port_range[0] > port_range[1]:
            return None, None
    
        port_list = range(int(port_range[0]), int(port_range[1]))
        random.shuffle(port_list)
        output = run(['iptables','-t','nat','-L','-w','3'])
        for port in port_list:
            try:
                # Make sure our port isn't already involved in some iptables
                # forwarding already.
                for line in output.split('\n'):
                    if not line is None and str(port) in line:
                        continue

                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.bind(("", port))
                break
            except Exception as e:
                pass
    else:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind(("", 0))
        port = sock.getsockname()[1]

    return port, sock


STATISTICS = []
class PerformanceStats:

    def __init__(self, record_stats=True, log_path='/tmp/timings.log', label=None):
        self.record_stats = record_stats
        self.log_path = log_path
        self.label = label
        return

    def __call__(self, func):
        def wrapper(*args, **kwargs):
            start = time.time()
            response = func(*args, **kwargs)
            finish = time.time()

            if self.label is None:
                label = func.__name__
            else:
                label = '{0}-{1}'.format(self.label, func.__name__)

            STATISTICS.append((label, start, finish-start))
            return response

        if self.record_stats == True:
            return wrapper
        else:
            return func

    
    @classmethod
    def add_stat(cls, statistic):
        STATISTICS.append(statistic)
        return

    @classmethod
    def write_stats(cls, stats_path):
        global STATISTICS
        with open(stats_path, 'a') as f:
            for s in STATISTICS:
                f.write('{0}\t{1}\t{2}\n'.format(s[0], s[1], s[2]))
        STATISTICS = []
        return

    @classmethod
    def clear(cls, stats_path):
        with open(stats_path, 'w') as f:
            pass
        return

    @classmethod
    def get_raw_stats(cls, stats_path, html=False):
        stats_list = []
        with open(stats_path, 'r') as f:
            stats = f.readlines()

        for entry in stats:
            stat = entry.split('\t')
            stats_list.append((stat[0].strip(), stat[1].strip(), stat[2].strip()))
        return stats_list

    @classmethod
    def get_calculated_stats(cls, stats):
        """
            Returns an array of tuples with (reqs/sec, avg. latency) measurements.
        """
        throughput = _calculate_throughput(stats)
        latency = _calculate_latency(stats)
        throughput_v_latency = _calculate_throughput_vs_latency(stats)
        return {'throughput': throughput, 'latency': latency,
                'throughput_v_latency': throughput_v_latency}


def _calculate_throughput_vs_latency(stat_list):
    results_map = {}
    start = None
    tmp_list = []

    try:
        index = 0
        offset = 1
        window_list = []
        while True:
            # Collect all requests for a given second
            window_list.append([])
            while stat_list[index+offset][1] - stat_list[index][1] < 1.0:
                window_list[len(window_list) - 1].append(stat_list[index])
                offset += 1
            index = index + offset
            offset = 1

    except IndexError as e:
        pass

    for window in window_list:

        if len(window) == 0:
            continue

        # Calculate the average latency over that second
        average_latency = sum(float(value[2]) for value in window)/len(window)
        if len(tmp_list) not in results_map:
            results_map[len(window)] = [average_latency]
        else:
            results_map[len(window)].append(average_latency)

#    for stat in stats_list:
#
#        if start is None:
#            start = float(stat[1])
#            tmp_list.append(stat)
#
#        else:
#            # Check if 1 second has ellapsed
#            if float(stat[1]) - start > 1.0:
#                start = None
#                average_latency = sum(float(value[2]) for value in tmp_list)/len(tmp_list)
#
#                if len(tmp_list) not in results_map:
#                    results_map[len(tmp_list)] = [average_latency]
#                else:
#                    results_map[len(tmp_list)].append(average_latency)
#
#                tmp_list = []
#
#            else:
#                tmp_list.append(stat)
            
        
    results = []
    for throughput, latency_list in results_map.iteritems():
        avg_latency = sum(latency_list)/len(latency_list)
        results.append((throughput, avg_latency))

    return sorted(results, key=lambda tup: tup[0])


def _calculate_throughput(stats_list):
    #results_map = {}

    start = None
    results = []
    tmp_list = []
    for stat in stats_list:

        if start is None:
            start = float(stat[1])
            tmp_list.append(stat)

        else:
            # Check if 1 second has ellapsed
            if float(stat[1]) - start > 1.0:
                results.append((start, len(tmp_list)))
                start = None
                tmp_list = []
                
                #if len(tmp_list) not in results_map:
                #    results_map[len(tmp_list)] = [average_latency]
                #else:
                #    results_map[len(tmp_list)].append(average_latency)

                #tmp_list = []

            else:
                tmp_list.append(stat)
            
        
        #results = []
        #for throughput, latency_list in results_map.iteritems():
        #    avg_latency = sum(latency_list)/len(latency_list)
        #    results.append((throughput, avg_latency))

    return sorted(results, key=lambda tup: tup[0])


def _calculate_latency(stats_list):
    return [(stat[1], stat[2]) for stat in stats_list]
