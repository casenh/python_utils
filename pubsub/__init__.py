#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

__author__ = 'Tarang Vaish'
__copyright__ = 'Copyright 2016, Tarang Vaish'


import errno
import os 

from abc import ABCMeta, abstractmethod
from python_utils.run import run


class PubSubUtil:
    __metaclass__ = ABCMeta 
    """
        Abstract Class for handling pub sub message pipe
    """

    def get_value():
        raise NotImplementedError        
  
    def set_value():
        raise NotImplementedError

    def clear():
        raise NotImplementedError
