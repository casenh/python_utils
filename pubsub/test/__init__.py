#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Tarang Vaish'
__copyright__ = 'Copyright 2016, Tarang Vaish'

import time

from python_utils.pubsub.redispubsub import RedisPubSub

def print_msg(msg):
    print(msg)

if __name__ == "__main__":
    r = RedisPubSub()
    args={}
    args['host'] = "127.0.0.1"
    args['port'] = 6379
    r.initialize(args)
    
    r.subscribe("test", print_msg)
    r.listen()

    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    r.publish("test", "Hello World")
    #r.publish("KILL", "now")
    try:
        while True:
            time.sleep(100)
    except KeyboardInterrupt as e:
        r.kill()
