#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Tarang Vaish'
__copyright__ = 'Copyright 2016, Tarang Vaish'

import redis
import signal
import threading

from python_utils.pubsub import PubSubUtil

class RedisPubSub(PubSubUtil):
    """ Class is a thin wrapper for pubsub library """
    expires = None

    @classmethod
    def initialize(cls, args):
        cls.client = redis.StrictRedis(host=args['host'], port=args['port'])
        cls.pubsub = cls.client.pubsub()
        cls.pubsub.subscribe("KILL")
        return

    def __init__(self):
        """ Initialize a Python PubSub class that uses redis """
        self.message_handlers={}
        signal.signal(signal.SIGTERM, self._kill)        
        return

    def _kill(self, signum, stack):
        print(signum)
        if signum == KeyboardInterrupt or signum == signal.SIGTERM:
            print(signum)
            self.kill()

    def kill(self):
        print("Killing pubsub")
        if self.thread:
            self.publish("KILL", "now")

    def publish(self, channel, key):
        """ Publish a key on a channel """
        self.client.publish(channel, key)

    def subscribe(self, channel, message_handler):
        """ Subscribe for messages on a channel """
        self.pubsub.subscribe(channel)
        self.message_handlers[channel] = message_handler
        print(self, "subscribed to channel ", channel)

    def unsubscribe(self):
        """ Unsubscribe for messages on a channel """
        self.pubsub.unsubscribe(channel)
        print(self, "unsubscribed from channel ", channel)
   
    def listen(self):
        """ Listen for messages on a channel """
        self.thread = threading.Thread(target=self._run_listen)
        self.thread.start()

    def _run_listen(self):
        for item in self.pubsub.listen():
            print(item)
            channel = item['channel']
            if channel in self.message_handlers:
                self.message_handlers[channel](item['data'])
            elif channel == "KILL" and item['data'] == "now":
                break
