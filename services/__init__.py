#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'

import python_utils.services.backup


def reset(reset_type="soft"):
    src.services.backup.model.reset(reset_type)
    return
