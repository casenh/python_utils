#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'

import dropbox
from flask import request, send_file
from flask_restful import Resource

from src import api, app
from src.services.backup.model import BackupService
from src.services.backup import error as backup_error 
from python_utils import log


class ApiBackup(Resource):

    def get(self, container_id):
        pass


    def post(self, user_id):
        ''' Add a Backup service for a particular user

            @command provider: Provider for backing up the data
            @command accessToken: Access Token to use with the provider
        '''
        print("Creating Backup Resource")
        try: 
            args = request.json
            service = BackupService.make(user_id, args)

        except backup_error.ArgumentError as e:
            return {'Result': 'Failure', \
                    'Reason': e.message }, 403

        return  {'Result': 'Success', \
                 'Container': service.info}, 200


    def put(self, container_id):
        pass


    def delete(self, container_id):
        pass


api.add_resource(ApiBackup, '/service/backup/<user_id>')
