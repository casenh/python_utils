#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = "Casen Hunger"
__copyright__ = "Copyright 2015, Casen Hunger"


class CreateError():
    pass


class DeleteError():
    pass


class GetError():
    pass


class CmdSyntaxError():
    pass


class DuplicateError():
    pass


class NoSuchContainer(Exception):
    def __init__(self, message=None):
        if message is None:
            message = 'Service does not exist for user'
        super(ArgumentError, self).__init__(message)
        return


class ArgumentError(Exception):

    def __init__(self, message):
        super(ArgumentError, self).__init__(message)
        return
