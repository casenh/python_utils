#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = 'Casen Hunger'
__copyright__ = 'Copyright 2015, Casen Hunger'


from datetime import datetime
import _mysql
import os

from python_utils.services.backup import error as backup_error
from python_utils.storage.dropbox import Dropbox
from python_utils.storage.s3 import S3

BACKUP_TBL = 'backupservice'

class BackupService:

    database = None
    DBConnection = None
    s3_bucket = None

    @classmethod
    def init(cls, database, DBConnection, s3bucket):
        cls.s3_bucket = s3bucket
        cls.DBConnection = DBConnection
        cls.database = database
        return
    
    @classmethod
    def make(cls, user_id, kwargs):
        '''
            Retreive an existing container if kwargs is None
            Create a new container otherwise

            @param userId: ID for the owner of container
            @param provider: Backup provider (Dropbox, GoogleDrive, S3)
            @param oauthToken: Access Token for using the service
            @param created: Time service was created
            @param modified: Time service was updated
            @param s3Bucket: Parameter for backing up to s3

            @return service: The created backup service object
        '''

        # Retreive an existing container
        if user_id is not None and kwargs is None:
            return BackupService(user_id, None)

        # Create or update container
        elif kwargs is not None:
            args = {}
            err = backup_error.ArgumentError
            if user_id is None:
                raise container_error.ArgumentError('Missing userId')
            args['userId'] = user_id
            if 'provider' not in kwargs:
                raise backup_error.ArgumentError('Missing Provider Type (Dropbox)')
            args['provider'] = kwargs['provider']
            if 'oauthToken' not in kwargs:
                raise backup_error.ArgumentError('Missing oauthToken for Backup Service')
            args['oauthToken'] = kwargs['oauthToken']

            args['s3Bucket'] = kwargs['s3Bucket']
            args['created'] = str(datetime.now())
            args['modified'] = str(datetime.now())

            return BackupService(user_id, args)


    def __init__(self, user_id, args):

            # Establish the connection to the database
            try:
                self.db = self.DBConnection(self.database)
            except container_error.NoSuchContainer as e:
                raise

            # Get existing container TODO: Throw exception
            if user_id is not None and args is None:
                try:
                    self.info = self.db.get_rows(BACKUP_TBL, {'userId': user_id})[0]
                except Exception as e:
                    # S3 storage is the default backup
                    self.db.create_row(BACKUP_TBL, {'userId': user_id, 'provider': 'S3'})
                    self.info = self.db.get_rows(BACKUP_TBL, {'userId': user_id})[0]
                return

            # Update existing container 
            if user_id is not None and args is not None:

                try:
                    # User may only have 1 backup service at a time
                    self.db.delete_row(BACKUP_TBL, {'userId': user_id})
                except Exception as e:
                    pass
           
                self.db.create_row(BACKUP_TBL, args)
                self.info = self.db.get_rows(BACKUP_TBL, {'userId': user_id})[0]
            
            return


    @property
    def provider(self):
        if self.info['provider'] == 'Dropbox':
            return Dropbox(self.info['oauthToken'])
        elif self.info['provider'] == 'S3':
            return S3(self.s3_bucket)


    @classmethod
    def reset(cls, reset_type="soft"):
        '''
            Delete all of the Containers and reset the database
        '''
        db = cls.DBConnection(cls.database)
        def reset_table():
            db.delete_table(BACKUP_TBL)
            db.create_table(BACKUP_TBL, userId='VARCHAR(36)', provider='VARCHAR(20)' 
                   , oauthToken='VARCHAR(128)', modified='VARCHAR(26)', created='VARCHAR(26)'
                   , s3Bucket='VARCHAR(128)')
    
        reset_table()
