#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Utilities to run external commands."""

from __future__ import print_function
from __future__ import absolute_import

import os
import pwd
import grp
import signal
import subprocess

DEVNULL = open(os.devnull, 'wb')


def drop_privileges(uid_name='nobody', gid_name='nobody'):
    """
        Drop privileges from root to a new user/group combo.

        @param uid_name: Name of user to change permissions to.
        @param gid_name: Group name to change permissions to.

        @throws: OSError
    """
    # If we aren't root, then don't do anything
    if os.getuid() != 0:
        print('Not root, so not dropping privileges')
        return

    running_uid = pwd.getpwnam(uid_name)[2]
    running_gid = grp.getgrnam(gid_name)[2]

    os.setgid(running_gid)
    os.setuid(running_uid)

    # Conservative umask setting
    os.umask(077)
    return


def run(cmd, env=None, stdin=None, stdout=None, stderr=None, async_=False, sudo=False, new_pgrp=False, shell=False):
    """
        Run a command. This function provides the ability to control where
        stdout, etc. goes.

        @param cmd: List of command arguments to run.
        @param env: Dictionary of environment variables to set.
        @param stdin: Where to assign stdin.
        @param stdout: Where to assign stdout.
        @param stderr: Where to assign stderr.
        @param async_: Whether to wait for the command to finish or not.
        @param new_pgrp: Whether to start the subprocess in new processor group. async_ needs to be true
    """
    # Setting stdin, stdout, and stderr is important here so signals like SIGINT are propagated
    # to the subprocess.
    if sudo == True:
        cmd.insert(0, 'sudo')

    # Can only pass in string arguments
    cmd = [str(entry) for entry in cmd]

    
    if async_ == True:
        pre_fn = None if new_pgrp is False else os.setsid
        process = subprocess.Popen(cmd, env=env, stdin=stdin, stdout=stdout, stderr=stderr, preexec_fn=pre_fn, shell=shell)
        return process
    else:
        return subprocess.check_output(cmd, env=env, stdin=stdin, stderr=stderr, shell=shell)

def run_no_out(cmd, env=None, async_=False, sudo=False, new_pgrp=False, shell=False):
    """
        Run a command. Similar to run() above but this supresses all
        output from the process.

        @param cmd: List of command arguments to run.
        @param env: Dictionary of environment variables to set.
        @param stdin: Where to assign stdin.
        @param stdout: Where to assign stdout.
        @param stderr: Where to assign stderr.
        @param async_: Whether to wait for the command to finish or not.
    """
    # Setting stdin, stdout, and stderr is important here so signals like SIGINT are propagated
    # to the subprocess.
    if sudo == True:
        cmd.insert(0, 'sudo')

    # Can only pass in string arguments
    cmd = [str(entry) for entry in cmd]

    if async_ == True:
        pre_fn = None if new_pgrp is False else os.setsid
        process = subprocess.Popen(cmd, env=env, stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL, preexec_fn=pre_fn, shell=shell)
        return process
    else:
        return subprocess.check_call(cmd, env=env, stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL, shell=shell)
